<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Основатели");
?>
		<div id="header" data-options='{ direction: "normal"}'>
			<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg3_1_11.jpg');height: 100%"></div>
			<div class="blc_desc_foto">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p><strong>Максим Панков</strong>, Золотая волна. Вечер на хребте Аибга. Сочинский национальный парк</p>
							<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
						</div>
					</div>
				</div><!--container-->		
			</div>		
		</div>
		<!--<div id="header" class="bg_main"></div>--><!--header-->

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="btnI">
						<a href="#" class="btn_i"></a>
					</div>
				</div>
			</div>
		</div><!--container-->	
		
		<div class="blc_bg_menu">
			<div class="bg_menu">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="logo"><a href="/"><img src="/html/images/mlogo.png" alt=""></a></div>
							<div class="blc_lang">
								<ul class="lang">
									<li class="active"><a href="#">ru</a></li>
									<li>/</li>
									<li><a href="#">en</a></li>
								</ul>
							</div>
							<div class="navbar navbar-default navbar-static-top">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
                                                                <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                                "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                        ), false );?> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="wrapper">
			
			<div class="cnt_in cnt_trio_persone">
				<div class="blc_trio_img">
					<div class="row mp">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm">
							<div class="blc_trio_img_desc">
								<!-- <ul class="drd_list board_directors">
									<li>
										<div class="desc">
											<img src="/html/images/p1.jpg" alt="">
											<div class="desc_bg">
												<div class="desc_txt">
													<a href="card_ng.php">
														<h5>Андриянкин Олег Владимирович</h5>
														<p>Финансовый директор</p>
														<span class="trn"></span>
													</a>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="desc">
											<img src="/html/images/p2.jpg" alt="">
											<div class="desc_bg">
												<div class="desc_txt">
													<a href="card_ng.php">
														<h5>Андриянкин Олег Владимирович</h5>
														<p>Финансовый директор</p>
														<span class="trn"></span>
													</a>
												</div>
											</div>
										</div>										
									</li>
									<li>
										<div class="desc">
											<img src="/html/images/p3.jpg" alt="">
											<div class="desc_bg">
												<div class="desc_txt">
													<a href="card_ng.php">
														<h5>Андриянкин Олег Владимирович</h5>
														<p>Финансовый директор</p>
														<span class="trn"></span>
													</a>
												</div>
											</div>
										</div>										
									</li>
								</ul> -->
								<?$APPLICATION->IncludeComponent( "bitrix:news.list", "osnovateli", array(
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"ADD_SECTIONS_CHAIN" => "N",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"AJAX_OPTION_HISTORY" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"CACHE_TIME" => "36000000",
									"CACHE_TYPE" => "A",
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"DISPLAY_DATE" => "N",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "Y",
									"DISPLAY_TOP_PAGER" => "N",
									"FIELD_CODE" => "",
									"FILTER_NAME" => "",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"IBLOCK_ID" => "1",
									"IBLOCK_TYPE" => "CONTENT",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"INCLUDE_SUBSECTIONS" => "Y",
									"MESSAGE_404" => "",
									"NEWS_COUNT" => "999",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => ".default",
									"PAGER_TITLE" => "Новости",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"PREVIEW_TRUNCATE_LEN" => "",
									"PROPERTY_CODE" => array( 0 => "DOLJNOST" ),
									"SET_BROWSER_TITLE" => "N",
									"SET_LAST_MODIFIED" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_STATUS_404" => "N",
									"SET_TITLE" => "N",
									"SHOW_404" => "N",
									"SORT_BY1" => "SORT",
									"SORT_BY2" => "ACTIVE_FROM",
									"SORT_ORDER1" => "DESC",
									"SORT_ORDER2" => "DESC",
									"COMPONENT_TEMPLATE" => "osnovateli"
								), false 	);?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div><!--wrapper-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>