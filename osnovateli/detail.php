<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Раскрытие информации");
?>
        <div id="header" data-options='{ direction: "normal"}'>
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg3_1_11.jpg');height: 100%"></div>
		<div class="blc_desc_foto">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Максим Панков</strong>, Золотая волна. Вечер на хребте Аибга. Сочинский национальный парк</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div><!--container-->		
		</div>		
	</div>
	<!--<div id="header" class="bg_main"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i btn_iW"></a>
				</div>
			</div>
		</div>
	</div><!--container-->
	</div><!--container-->


	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?> 							
						</div><!--/.nav-collapse -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper">
		<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"osnovateli_detail", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => $_REQUEST["CODE_NEWS"],
		"ELEMENT_ID" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "CONTENT",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "osnovateli_detail"
	),
	false
);?>
		<!-- <div class="cnt_in cnt_persone">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="go_back">
							<a href="/osnovateli/" class="back_section">
								Назад к разделу
							</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="persone_img">
							<img src="/html/images/p1s.jpg" alt="">
						</div>
						<h6>Шишханов Микаил Османович</h6>
						<p>В 1995 году с отличием окончил магистратуру факультета экономики и права Университета дружбы народов имени Патриса Лумумбы, а в 2000 году — Финансовую академию при Правительстве РФ. </p>
						<p>В 1998 году защитил диссертацию на соискание учёной степени кандидата юридических наук, а в 2002 году — доктора экономических наук.</p>
						<p>Член-корреспондент Российской академии естественных наук.</p>
						<p>Начиная с 1992 года, Микаил Шишханов совмещал учебу с работой в АОЗТ «Промышленно-финансовая компания «БИН», где за короткий срок занял пост генерального директора. </p>
						<p>В июне 1994 года перешёл на работу в Акционерный коммерческий банк «БИН», в котором работал на должности начальника управления по организации межбанковских связей и представителей банка в регионах. В сентябре 1994 года был назначен Вице-президентом и избран заместителем Председателя Правления Банка. С января 1996 года по июль 2015 года — президент, председатель правления ПАО «БИНБАНК». </p>
						<p>В настоящее время возглавляет Совет директоров БИНБАНКа и является Председателем Правления ПАО «МДМ Банк».</p>
						<p>Г-н Шишханов активно участвует в жизни банковского сообщества, являясь членом Совета Ассоциации Российских Банков и Московского Банковского Союза.</p>
						<p>Награжден орденом «Золотая Звезда за Верность России», орденом Сергия Радонежского III степени Русской Православной церкви и Почетным знаком Ассоциации Российских Банков «За заслуги перед банковским сообществом».</p>
					</div>
				</div>
			</div><!--container
		</div> -->

	</div><!--wrapper-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>