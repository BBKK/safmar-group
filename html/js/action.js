var headerHeightstart = 493;
var headerTransitionY = 0;
var breakAnimation = false;
var news_ajax = false;
//var destroy_ajax = false;
//var after_multi_ajax_news = false;


jQuery(document).ready(function () {
	$('.prev_q_singl, .next_q_singl, .next_q, .prev_q, .quartal').on('click', function(event) {
		event.preventDefault();
	});

	$("#slider_year").slider({
		range: "min",
		value: 2016,
		min: parseInt($("#inp_year").attr('min-year')),
		max: parseInt($("#inp_year").attr('max-year')),
		slide: function (event, ui) {
			$("#inp_year").val(ui.value).trigger('change');
			//makeNewsajax();
		}
	});

	$("#inp_year").val($("#slider_year").slider("value"));

	$('#inp_year').on('change', function(){
		console.log('make');
		makeNewsajax();
	});

        /*$('body').on('change', 'input#inp_year', function(){
                console.log('234234');
                get_news();
                return false;
        });*/

	$('.prev_q_singl, .next_q_singl').on('click', function(event) {
		event.preventDefault();
		var direction = $(this).hasClass('prev_q_singl') ? 'right' : 'left';
		makeSingleNewajax(direction, $(this));
	});

	function animateSingleNewChange(direction) {
		console.log(1);
		$('.news_block_item').effect({
			'effect': 'drop',
			'easing': 'easeOutCubic',
			'duration': 800,
			'direction': direction
		}, function() {
			$('.news_block_item').empty();
		})


		/*$('.sigle_new_descr_date').html('255 hjbhjb');
		 $('.sigle_new_descr_txt').html('истая прибыль БИНБАНКа по итогам 2015 года выросла ');
		 $('.blc_all_desc_news_txt').html(' Таковы данные отчетности по российским стандартам бухгалтерск прибыль БИНБАНКа по итогам 2015 года выросла ');
		 */
		//$('.news_block_item').fadeIn(500);
	}

	function animateNextNew() {
                console.log(1);
		$('.dd_list').effect({
			'effect': 'drop',
			'easing': 'easeOutCubic',
			'duration': 1600,
			'direction': 'right'
		})
		/*$('.sigle_new_descr_date').html('255 hjbhjb');
		$('.sigle_new_descr_txt').html('истая прибыль БИНБАНКа по итогам 2015 года выросла ');
		$('.blc_all_desc_news_txt').html(' Таковы данные отчетности по российским стандартам бухгалтерск прибыль БИНБАНКа по итогам 2015 года выросла ');
*/
		$('.dd_list').fadeIn(500);
	}

	function animatePrevNew() {
                console.log(2);
		$('.dd_list').effect({
			'effect': 'drop',
			'easing': 'easeOutCubic',
			'duration': 1600,
			'direction': 'left'
		})
		/*$('.sigle_new_descr_date').html('255 hjbhjb');
		$('.sigle_new_descr_txt').html('истая прибыль БИНБАНКа по итогам 2015 года выросла ');
		$('.blc_all_desc_news_txt').html(' Таковы данные отчетности по российским стандартам бухгалтерск прибыль БИНБАНКа по итогам 2015 года выросла ');
*/
		$('.dd_list').fadeIn(500);
	}

	$('.quartal').on('click', function() {

		$('.quartal').parents('.sele_q').removeClass('active');
		$(this).parents('.sele_q').addClass('active');

		if( !news_ajax ) {
			//	destroy_ajax = $(this);
			//	} else {
			makeNewsajax();
		}
	})

	$('.next_q').on('click', function() {

		//if(!after_multi_ajax_news) {

		//}

		if( !news_ajax ) {
		//	destroy_ajax = $(this);
		//} else {
			var year = parseInt($('#inp_year').val());
			if (year < parseInt($("#inp_year").attr('max-year'))) {
				$('#inp_year').val(year + 1);
				$('#slider_year').slider("value", (year+1));
			}

			makeNewsajax();

			//if(!after_multi_ajax_news) {
				$('.quartal').parents('.era_list_years li.sele_q').fadeOut(900).fadeIn(900);
			//} else {
			//	after_multi_ajax_news = false;
			//}
		}

	})

	$('.prev_q').on('click', function() {

		//if(!after_multi_ajax_news) {

		//}

		if( !news_ajax ) {
		//	destroy_ajax = $(this);
		//} else {

			var year = parseInt($('#inp_year').val());
			if (year > parseInt($("#inp_year").attr('min-year'))) {
				$('#inp_year').val(year - 1);
				$('#slider_year').slider("value", (year-1));
			}

			makeNewsajax();

			//if(!after_multi_ajax_news) {
				$('.quartal').parents('.era_list_years li.sele_q').fadeOut(900).fadeIn(900);
			//} else {
			//	after_multi_ajax_news = false;
			//}
		}


	})
	
	function makeNewsajax() {

                if(!news_ajax){
		news_ajax = true;
                        var id = $('.sele_q.active .quartal').attr('data-attr') == undefined ? 0 : $('.sele_q.active .quartal').attr('data-attr');
		var year = $('#inp_year').val();

		$.ajax({
				method: "POST",
                                        url: "/includes/news_list.php",
				data: { id: id, year: year }
			})
			.done(function( msg ) {

				//alert( "Data Saved: " + id + 'year ' +year );
				$('.blc_link_on_news').height($('.blc_link_on_news').height());
                                        $('.blc_link_on_news').fadeOut(400).empty();
				setTimeout(function() {
                                               // if(!destroy_ajax) {
						news_ajax = false;
                                                        if(msg.type === true){
                                                                $('.blc_link_on_news').html(msg.text);
                                                                $('.blc_link_on_news').css('height', 'auto');

                                                        }

											$('.blc_link_on_news').fadeIn(400);

                                                        /*var html = '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'+
										'<span class="date">225 января 2016</span>'+
										'<p>Чистая прибыль БИНБАНКа по итогам 2015 года выросла на 139%, превысив 4,5 млрд рублей</p>'+
											'<a href="news_detail.html">Подробнее</a>'+
                                                                                '</div>';*/

                                               // } else {
                                                //        var element = destroy_ajax;
                                                //        destroy_ajax = false;
                                                //        news_ajax = false;
                                                     //   after_multi_ajax_news = true;
                                                 //       $(element).trigger('click');
                                               // }
                                        }, 1000);
                                });
                }
	}

	function makeSingleNewajax(direction, object) {

		if(!news_ajax){
			news_ajax = true;
			var id = $(object).attr('href');

			$.ajax({
					method: "POST",
					url: "/includes/news_detail.php",
					data: { CODE: encodeURIComponent(id) }
				})
				.done(function( msg ) {

					//alert( "Data Saved: " + id + 'year ' +year );
					$('.news_block_item').height($('.news_block_item').height());

					animateSingleNewChange(direction);
					setTimeout(function() {
						// if(!destroy_ajax) {
						news_ajax = false;
						if(msg.type === true){
							$('.news_block_item').html(msg.text);
							$('.news_block_item').css('height', 'auto');


						}

						if(msg.next === null) {
							$('.next_q_singl').addClass('hide');
						} else {
							$('.next_q_singl').removeClass('hide');
							$('.next_q_singl').attr('href', msg.next);
					}

						if(msg.pref === null) {
							$('.prev_q_singl').addClass('hide');
						} else {
							$('.prev_q_singl').removeClass('hide');
							$('.prev_q_singl').attr('href', msg.pref);
						}

						$('.news_block_item').fadeIn(200);
				}, 800);
			});
	}
	}

	$(window).resize(function () {
		MenuBorderProccess();
	});
	//$('.pensioner_admin_circle_main .container').css('height', '450');
	//$('.tabs__content .number_participants').css('height', '374');

	//if ($('#submenu-support').length > 0) {
	//	$('body').css('height', parseInt($('body').height()) + 52);
	//}


	BannerAnimationProcess();
	//CheckSubmenuPosition();
	//CheckSubmenuPositionForPartner();
	CheckFiltersPosition();
	MenuBorderProccess();
	InitSlideCircles();
	initEvents();
	InitPlugins();
	InitFeedbackSteps();

	setTimeout(function () {
		SlideDirrectors();
	}, 1000);
	//CloseAllContent();
	//var topLogo = $('.logo').innerHeight() + 30;
	jQuery(window).scroll(function (e) {
		//console.log(e);
		//		if(jQuery(window).scrollTop() >= topLogo){
		//			$('.logo').fadeOut(200);
		//			//jQuery('.logo_fix').addClass('active');
		//			$('.logo_fix').fadeIn(200);
		//		} 
		//        else {
		//        	//jQuery('.logo_fix').removeClass('active'); 
		//			$('.logo').fadeIn(200);
		//			$('.logo_fix').fadeOut(200);
		//		}

		if ($('body').scrollTop() > 100) {
			if ($('.bg_menu ul.navbar-nav.fixmenu>li.active').length > 0) {
				$('.blc_bg_menu').css('margin-top', -parseInt($('.bg_menu').height() + 4));
			}
		} else {
			$('.blc_bg_menu').css('margin-top', 0);

		}

		if ($('.tabs__content.active').length > 0) {
			var elementTop = $('.tabs__content.active').offset().top;
			if (jQuery(window).scrollTop() > elementTop - (elementTop / 2) - 300 && !!!$('.tabs__content.active').attr('data-is-showed')) {
				showCirclesAnimation($('.tabs__content.active'));
			}
		}
		CheckFiltersPosition();
		//CheckSubmenuPosition();
		//CheckSubmenuPositionForPartner();
	});

});

function InitSlideCircles() {
	var container = $('#slideCircles');
	var current = container.find('.tabs__content.active');
	var next = current.next();
	var nextMarginTop = '';
	var currMarginTop = -current.height();
	if (next.length <= 0) {
		next = container.find('.tabs__content:first');
		nextMarginTop = -next.height();
		currMarginTop = '';
	}

	var prev = current.prev();
	if (prev.length <= 0) {
		prev = container.find('.tabs__content:last');
	}

	setTimeout(function () {
		NextSlide(current, next, nextMarginTop, currMarginTop);
	}, 4500);
}

function SlideToIndex(index) {
	var container = $('#slideCircles');
	var current = container.find('.tabs__content.active');
	var next = container.find('.tabs__content[data-index=' + index + ']');
	var nextMarginTop = '';
	var currMarginTop = -current.height();
	var currentIndex = parseInt(current.attr('data-index'));
	if (index < currentIndex) {
		nextMarginTop = -next.height();
		currMarginTop = '';
	}
	breakAnimation = true;

	setTimeout(function () {
		breakAnimation = false;
		NextSlide(current, next, nextMarginTop, currMarginTop);
		setTimeout(function () {
			breakAnimation = true;
			$('.play_animate').addClass('pause');
		}, 100);
	}, 500);


}

function NextSlide(current, next, nextMarginTop, currMarginTop) {
	var wWidth = $(window).width();
	//if (!$('.play_animate').hasClass('pause')) {
	if (!breakAnimation) {
		next.css('left', wWidth)
		.css('opacity', 0)
		.css('display', 'block')
		.css('margin-top', currMarginTop);;
		current.css('margin-top', nextMarginTop);
		current.stop().animate({ 'opacity': 0, 'left': -wWidth }, {
			easing: "linear",
			duration: 800,
			complete: function () {
				current.removeClass('active');
				current.css('display', 'none');
				next.css('margin-top', '');
				current.css('margin-top', '');
				$('.tabs_corp .part_cp li.active').removeClass('active');
			}
		});
		next.stop().animate({ 'opacity': 1, 'left': 0 }, {
			easing: "linear",
			duration: 800,
			complete: function () {
				$(this).addClass('active');
				var index = $(this).attr('data-index');
				$('.tabs_corp .part_cp li[data-index="' + index + '"]').addClass('active');
				//								var topOfCircles = parseInt($('.number_participants').offset().top);
				//								if (topOfCircles > 700) {
				//									var topval = parseInt($('.tabs__content.tc' + index).parents('div.row').offset().top) - parseInt($('.tab_corporate').outerHeight());
				//									$.scrollTo(topval, 500);
				//								}
				showCirclesAnimation($('.tabs__content.tc' + index));
				if (!$('.play_animate').hasClass('pause')) {
					setTimeout(function () {
						InitSlideCircles();
					}, 500);
				}
			}
		});
	}
}

function InitSlideCircles1() {
	var slideCirclesSlider = new Swiper('#slideCircles', {
		autoplay: 1,
		speed: 2200,
		slidesPerView: 3,
		loop: true,
		spaceBetween: 0,
		effect: 'slide',
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		observer: true,
		observeParents: true,
		breakpoints: {
			// when window width is <= 320px
			384: {
				slidesPerView: 1,
				spaceBetween: 0
			},
			// when window width is <= 480px
			580: {
				slidesPerView: 2,
				spaceBetween: 0
			},
			// when window width is <= 640px
			767: {
				slidesPerView: 3,
				spaceBetween: 0
			},
			1200: {
				slidesPerView: 4,
				spaceBetween: 0
			}
		}
	});
}

function SlideDirrectors() {
	var container = $('#directors2');
	if (container.length > 0) {
		container.css('overflow', 'hidden');
		var items = container.find('li');
		var itemHeight = container.find('li:first').height();
		//var itemWidth = parseInt(container.find('li:first').css('width').replace('px',''));
		var containerWidth = container.find('ul').width();
		var itemWidth = Math.round(containerWidth * 0.333);
		var visibleItemsCount = Math.round(containerWidth / itemWidth);
		var totalItemsCount = items.length;
		//var itemWidth = Math.round(containerWidth/visibleItemsCount);
		container.find('li').css('width', itemWidth);
		if (visibleItemsCount < totalItemsCount) {
			//container.css('position', 'relative');
			//.css('width', itemWidth * totalItemsCount);
			container.find('ul').css('width', itemWidth * totalItemsCount)
					.css('position', 'relative');
			container.find('li').css('display', 'inline-block')
				.css('text-align', 'left')
				.css('float', 'left')
				.css('position', 'relative');//.css('width', itemWidth*totalItemsCount);
		}
		var currentMarginLeft = 0;// Math.abs(parseInt(container.find('ul').css('margin-left').replace('px', '')));

		$('#directors2').on('DOMMouseScroll mousewheel', function (e) {
			e = e ? e : window.event;
			if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
				if (currentMarginLeft < ((totalItemsCount - visibleItemsCount) * itemWidth)) {
					window.event.preventDefault();
					currentMarginLeft = currentMarginLeft + itemWidth;
					container.find('ul').animate({
						'margin-left': -(currentMarginLeft),
					},
					{
						//easing: "easeInQuart",
						duration: 500,
						complete: function () {
						}
					});

					if (e.stopPropagation) {
						e.stopPropagation();
					}
					if (e.preventDefault) {
						e.preventDefault();
					}
					e.cancelBubble = true;
					e.cancel = true;
					e.returnValue = false;
					return false;
				}
			} else {
				if (currentMarginLeft >= ((totalItemsCount - visibleItemsCount) * itemWidth)) {
					window.event.preventDefault();
					currentMarginLeft = currentMarginLeft - itemWidth;
					container.find('ul').animate({
						'margin-left': -(currentMarginLeft),
					},
					{
						//easing: "easeInQuart",
						duration: 500,
						complete: function () {
						}
					});
					if (e.stopPropagation) {
						e.stopPropagation();
					}
					if (e.preventDefault) {
						e.preventDefault();
					}
					e.cancelBubble = true;
					e.cancel = true;
					e.returnValue = false;
					return false;
					//container.find('ul').css('margin-left', -(currentMarginLeft - itemWidth));
				}
			};
		});
	}
}

function MenuBorderProccess() {
	var magicPrefix = 25;
	var prefix = $('.logo').width() + magicPrefix;
	var fiestPrefixOffset = $('.bg_menu ul.navbar-nav>li:first').offset().left;
	var underliingSliderLock = false;
	$('span.line_menu').css('left', $('.bg_menu ul.navbar-nav>li.active').offset().left - fiestPrefixOffset + prefix).css('width', $('.bg_menu ul.navbar-nav>li.active').outerWidth());
	$('.bg_menu ul.navbar-nav:not(.fixmenu)>li>div.navIn').css('display', 'none');
	if ($('.bg_menu ul.navbar-nav.fixmenu').length > 0) {
		$('.bg_menu ul.navbar-nav.fixmenu>li>div.navIn').css('display', 'none');
		$('.bg_menu ul.navbar-nav.fixmenu>li.active>div.navIn').css('display', 'block');
		$('.bg_menu ul.navbar-nav.fixmenu>li.active').addClass('hover');
		$('.bg_menu').addClass('active');
	}

	var startSubmenuAnimation = false;
	$('.bg_menu ul.navbar-nav:not(.fixmenu)>li').off('mouseleave').on('mouseleave', function () {
		setTimeout(function () {
			underliingSliderLock = false;
		}, 300);
		$(this).removeAttr('data-active');
	});
	$('.bg_menu ul.navbar-nav:not(.fixmenu)>li').off('mouseenter').on('mouseenter', function () {
		var liElement = $(this);
		liElement.attr('data-active', 'true');
		if ($(this).find('ul').length > 0) {
			//$(this).find('div.navIn').css('display', 'block');
			//				setTimeout(function () {
			//					$('.bg_menu').addClass('active');
			//				}, 500);
			setTimeout(function () {
				if (!liElement.hasClass('hover')) {
					setTimeout(function () {
						$('.bg_menu ul.navbar-nav:not(.fixmenu)>li').removeClass('hover');
					}, 10);
					setTimeout(function () {
						$('.bg_menu ul.navbar-nav:not(.fixmenu)>li>div.navIn').css('display', 'none');
					}, 30);
					setTimeout(function () {
						liElement.find('div.navIn').css('display', 'block');
						//setTimeout(function () {
						//	if (liElement.find('ul').length <= 0) {
						//		$('.bg_menu').removeClass('active');
						//	}
						//}, 500);
						//liElement.find('div.navIn').addClass('open');
						setTimeout(function () {
							
							//if ($('.blc_fix_fltr').hasClass("fix_fltr")) {
							//	$('.bg_menu').addClass('not_opst');
							//} else {
							//	$('.bg_menu').removeClass('not_opst');
							//}
							$('.bg_menu').addClass('active');
							liElement.addClass('hover');
						}, 100);
					}, 50);
				}
			}, 500);
		} else {
			//if ($('.blc_fix_fltr').hasClass("fix_fltr")) {
			//	$('.bg_menu').removeClass('not_opst');
			//}
			$('.bg_menu').removeClass('active');
			$('.bg_menu ul.navbar-nav:not(.fixmenu)>li').removeClass('hover');
			//setTimeout(function () {
			$('.bg_menu ul.navbar-nav:not(.fixmenu)>li>div.navIn').css('display', 'none');
			//}, 100);
			//$('.bg_menu ul.navbar-nav:not(.fixmenu)>li>ul').css('display', 'none');
		}
		//}, 300);



		//var topPosition = $(this).offset().top;
		//var heightElement = $(this).outerHeight();
		//if (e.clientY > topPosition && e.clientY < (topPosition + heightElement)) {
		setTimeout(function () {
			//if (!underliingSliderLock) {
			underliingSliderLock = true;
			setTimeout(function () {
				underliingSliderLock = false;
			}, 600);
			var liElementActive = $('.bg_menu ul.navbar-nav:not(.fixmenu)>li[data-active="true"]');
			var leftElement = $('.bg_menu ul.navbar-nav>li.active').offset().left;
			var widthElement = $('.bg_menu ul.navbar-nav>li.active').outerWidth();
			if (liElementActive.length > 0) {
				leftElement = liElementActive.offset().left;
				widthElement = liElementActive.outerWidth();
			}
			$('span.line_menu').animate({
				'left': leftElement - fiestPrefixOffset + prefix,
				'width': widthElement
			},
			{
				//easing: "easeInQuart",
				duration: 500,
				complete: function () {
					underliingSliderLock = false;
				}
			});
			//}
		}, 600);
	});
	//$('.bg_menu ul.navbar-nav').mouseleave(function (e) {
	$('.bg_menu .navbar-collapse ul.navbar-nav:not(.fixmenu)').off('mouseleave').on('mouseleave', function (e) {
		var topPosition = Math.abs($(this).offset().top- $(window).scrollTop());
		var heightElement = $(this).outerHeight();
		var heightAppendix = 0;
		if ($('.bg_menu ul.navbar-nav:not(.fixmenu)>li.hover ul.nav_in').length > 0) {
			heightAppendix = 75;
		}
		if (e.clientY < (topPosition) || e.clientY > (topPosition + heightElement + heightAppendix)) {
			setTimeout(function () {
				//if ($('.bg_menu ul.navbar-nav:not(.fixmenu)>li.hover').length <= 0) {
				$('.bg_menu ul.navbar-nav:not(.fixmenu)>li.hover').removeClass('hover');
				$('.bg_menu').removeClass('active');
				$('.bg_menu ul.navbar-nav:not(.fixmenu)>li>div.navIn').css('display', 'none');
				//}
			}, 600);
		}
		setTimeout(function () {
			underliingSliderLock = false;
		}, 300);

		if (e.clientY < (topPosition) || e.clientY > (topPosition + heightElement + heightAppendix)) {
			if (!underliingSliderLock) {
				underliingSliderLock = true;
				//var prefix = 288;

				$('span.line_menu').animate({
					'left': $('.bg_menu ul.navbar-nav>li.active').offset().left - fiestPrefixOffset + prefix,
					'width': $('.bg_menu ul.navbar-nav>li.active').outerWidth()
				}, {
					easing: "easeInQuart",
					duration: 500,
					complete: function () {
						underliingSliderLock = false;
					}
				});
			}
		}
	});

	//$('.bg_menu ul.navbar-nav>li').on('mouseenter', function (e) {
	//	//var topPosition = $(this).offset().top;
	//	//var heightElement = $(this).outerHeight();
	//	//if (e.clientY > topPosition && e.clientY < (topPosition + heightElement)) {
	//	if (!underliingSliderLock) {
	//		underliingSliderLock = true;
	//		setTimeout(function () {
	//			underliingSliderLock = false;
	//		}, 600);
	//		$('span.line_menu').animate({
	//			'left': $(this).offset().left - fiestPrefixOffset + prefix,
	//			'width': $(this).outerWidth()
	//		},
	//		{
	//			easing: "easeInQuart",
	//			duration: 500,
	//			complete: function () {
	//				underliingSliderLock = false;
	//			}
	//		});
	//	}
	//	//}
	//});
	//$('.bg_menu ul.navbar-nav').on('mouseleave', function (e) {
	//	setTimeout(function () {
	//		underliingSliderLock = false;
	//	}, 300);
	//	var topPosition = $(this).offset().top;
	//	var heightElement = $(this).outerHeight();
	//	if (e.clientY < topPosition || e.clientY > (topPosition + heightElement)) {
	//		if (!underliingSliderLock) {
	//			underliingSliderLock = true;
	//			//var prefix = 288;
	//
	//			$('span.line_menu').animate({
	//				'left': $('.bg_menu ul.navbar-nav>li.active').offset().left - fiestPrefixOffset + prefix,
	//				'width': $('.bg_menu ul.navbar-nav>li.active').outerWidth()
	//			}, {
	//				easing: "easeInQuart",
	//				duration: 500,
	//				complete: function () {
	//					underliingSliderLock = false;
	//				}
	//			});
	//		}
	//	}
	//});
}

function ShowBannerMinimized() {
	var banner = $('#header');
	if ($('.blc_bg_menu .bg_menu').attr('data-active') == 'true') {
		if ($('.blc_fix_fltr').hasClass("fix_fltr")) {
			$('.bg_menu').addClass('not_opst');
		}
		$('.blc_bg_menu .bg_menu').addClass('active');

	}

	$('.blc_desc_foto').addClass('min');
	//$('.blc_bg_menu').removeClass('hide_top_menu');

	banner.addClass('bg_header_prepend');
	banner.css('height', Math.round(parseInt($(window).height()) * 0.75));
	banner.find('.divimage').addClass('bg_image_prepend');
	//setTimeout(function () {
	banner.css('height', '495');
	//	banner.addClass('prepended');
	setTimeout(function () {
		//$('#header').css('display', 'none');
		//$('#header_dynamic').css('display', '');
		$('#header').parents('div').addClass('bg_main_wrapper');
		//banner.find('.divimage').css('height', $(window).height());
		$('#header').removeClass('dzstop');
		//banner.find('.divimage').addClass('bg_image_prepend');
		//banner.find('.divimage').css('background-position-y', '100%');


		dzsprx_init('#header', { direction: "normal", mode_scroll: "fromtop" });
	}, 1200);
	//}, 750);
}

function ShowBannerFull() {

	var banner = $('#header');
	banner.parents('div').removeClass('bg_main_wrapper');
	//banner.find('.divimage').css('height', '100%');
	var bannerStaticHeight = banner.attr('data-height');

	banner.find('.divimage').css('height',  $(window).height()+50);

	//$('#header').css('display', '');
	//$('#header_dynamic').css('display', 'none');
	//$('.blc_bg_menu .bg_menu').removeClass('active');
	//$('.blc_bg_menu').addClass('hide_top_menu');
	if (!!!bannerStaticHeight) {
		var bannerHeight = banner.outerHeight();
		banner.attr('data-height', bannerHeight);
	}

	if ($('.blc_bg_menu .bg_menu').hasClass('active')) {
		$('.blc_bg_menu .bg_menu').attr('data-active', true);
	}

	if (!!!bannerStaticHeight) {
		banner.css('height', $(window).height() + 50);
	} else {
		banner.css('height', $(window).height());
	}

	$('.blc_desc_foto').removeClass('min');
	//$('.blc_bg_menu').addClass('hide_top_menu');
	$('.blc_bg_menu .bg_menu').removeClass('active');
	if ($('.blc_fix_fltr').hasClass("fix_fltr")) {
		$('.bg_menu').removeClass('not_opst');
	}
	banner.find('.divimage').removeClass('bg_image_prepend');
}

function BannerAnimationProcess() {
	ShowBannerFull();
	////$('.blc_bg_menu .bg_menu').removeClass('active');
	////$('.blc_bg_menu').addClass('hide_top_menu');
	//var bannerStaticHeight = banner.attr('data-height');
	//if (!!!bannerStaticHeight) {
	//	var bannerHeight = banner.outerHeight();
	//	banner.attr('data-height', bannerHeight);
	//}
	//banner.css('height', $(window).height());
	////banner.css('background-position-y', '100%')

	setTimeout(function () {
		ShowBannerMinimized();
		//banner.stop().animate({ 'background-position-y': 0 }, {
		//	step: function(now, fx) {
		//		 $(this).css('background-position-y', Math.abs(now - 100) + '%');
		//	},
		//	easing: "linear",
		//	duration: 800
		//});
	}, 1500);
	$('a.btn_i').click(function () {
		if (!$('.blc_desc_foto').hasClass('min')) {
			ShowBannerMinimized();
		} else {
			$.scrollTo(0, 500);
			setTimeout(function () {
				$('#header').addClass('dzstop');
				//$('.divimage').css('transform', 'translate3d(0px, 0px, 0px)');

				setTimeout(function () {
					ShowBannerFull();
				}, 200);
			}, 200);
		}
	});
}

function showCirclesAnimation(container) {
	if (!!!$(container).attr('data-is-showed')) {
		container.attr('data-is-showed', true);
		var index = parseInt(container.attr('data-index'));
		if (index == 1 || index == 2) {
			ShowCircle(index, 1, 45, 14, container, '#ea491e');
			ShowCircle(index, 2, 93, 14, container, '#ea491e');
			ShowCircle(index, 3, 153, 14, container, '#ea491e');
			ShowCircle(index, 4, 75, 14, container, '#c6c6c6');
		}
		else if (index == 3) {
			ShowCircle(index, 1, 45, 14, container, '#ea491e');
			ShowCircle(index, 2, 93, 14, container, '#c6c6c6');
			ShowCircle(index, 3, 153, 14, container, '#ea491e');
			ShowCircle(index, 4, 45, 14, container, '#c6c6c6');
			ShowCircle(index, 5, 125, 14, container, '#ea491e');
		}
		else if (index == 4) {
			for (var i = 1; i <= 8; i++) {
				$('.tabs__content.tc4 .blc_cut_circle .cr' + i).css('opacity', '0');
				$('.tabs__content.tc4 .blc_cut_circle .statistic.st' + i).css('opacity', '0');
			}
			ShowNext(1, 8, 6);
		}
		else if (index == 5) {
			ShowCircle(index, 1, 32, 14, container, '#ea491e');
			ShowCircle(index, 2, 45, 14, container, '#c6c6c6');
			ShowCircle(index, 3, 100, 14, container, '#ea491e');
			ShowCircle(index, 4, 125, 14, container, '#ea491e');

		}
	}
}

function ShowNext(i, limit, statisticLimit) {
	$('.tabs__content.tc4 .blc_cut_circle .cr' + i).stop().animate({ 'opacity': 1 }, {
		easing: "linear",
		duration: 400,
		complete: function () {
			//								if(i+1<=limit)
			//								{
			//									ShowNext(i+1,limit,statisticLimit);
			//								}
		}
	});
	$('.tabs__content.tc4 .blc_cut_circle .statistic.st' + (statisticLimit - i + 1)).stop().animate({ 'opacity': 1 }, {
		easing: "linear",
		duration: 400
	});

	setTimeout(function () {
		if (i + 1 <= limit) {
			ShowNext(i + 1, limit, statisticLimit);
		}
	}, 200);
}

function ShowCircle(index, postfix, radius, width, container, secondColor) {
	var dataContainer = container.find('#pcircles-' + index + postfix);
	var text = dataContainer.attr('data-text');
	var mesure = dataContainer.attr('data-mesure');
	var dataValueString = dataContainer.attr('data-value');
	var data = dataValueString.split('.');
	var dataValuePostfix = '';
	if (data.length > 1) {
		dataValuePostfix = ',' + data[1];
	}


	var datavalue = parseInt(dataContainer.attr('data-value'));
	var datapostfix = dataContainer.attr('data-postfix');
	Circles.create({
		id: 'pcircles-' + index + postfix,
		value: datavalue,
		maxValue: datavalue,
		radius: radius,
		duration: 800,
		width: width,
		colors: ['#ffffff', secondColor],
		text: function (value) { return "<p>" + text + ", <span>" + mesure + "</span> <span class='str'>" + datapostfix + " </span><span class='number'>" + FormatNumber(value, dataValuePostfix) + "</span></p>"; }
	});
}

function FormatNumber(value, dataValuePostfix) {
	var strVal = value.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	if (Math.round(value) == value) {
		strVal += dataValuePostfix;
	}

	return strVal
}


function initEvents() {
	$('.ver_tabs li').unbind("click").bind("click", function () {
		if ($(this).is('.open')) {
			CloseContent($(this));
		} else {
			OpenContent($(this));
		}
	});
	$("#submenu-support .hor_tabs li").unbind("click").bind("click", function () {
		if ($(this).attr('data-target') != "all-services") {
			//$("#submenu-support .hor_tabs li").removeClass("active");
			if ($(this).hasClass("active")) {
				$(this).removeClass("active")
			} else {
				$("#submenu-support .hor_tabs li[data-target='all-services']").removeClass("active");
				$(this).addClass("active");
			}

			var element = $("#" + $(this).attr('data-target'));

			if ($(this).hasClass("active")) {
				$("#submenu-support .hor_tabs li:not(.active)").each(function () {
					CloseContent($("#" + $(this).attr('data-target')));
				});
				OpenContent(element);
			} else {
				CloseContent(element);
			}
			setTimeout(function () {
				var additional = $('#submenu-support').innerHeight() + $('.blc_bg_menu').innerHeight() + 25;
				$.scrollTo(element.offset().top - additional, 500);
			}, 400);


			//$.scrollTo(element.position().top+additional, 500);

		} else {
			var state = $(this).hasClass("active");

			$("#submenu-support .hor_tabs li").removeClass("active");

			if (state) {
				$(this).removeClass("active")
			} else {
				$(this).addClass("active");
			}
			var additional = $('#submenu-support').innerHeight() + $('.blc_bg_menu').innerHeight() + 25;
			$.scrollTo($(".ver_tabs").offset().top - additional, 500);
			if ($(this).hasClass("active")) {
				OpenAllContent();
			} else {
				CloseAllContent();
			}
		}
	});

	if ($('input').is('.phone')) {
		$('input.phone').mask('+7 (999) 999-99-99');
		jQuery.validator.addMethod('phoneRU', function (value, element) {
			return this.optional(element) || /^\+7 \(([0-9]){3}\) ([0-9]){3}-([0-9]){2}-([0-9]){2}$/.test(value) || /^([0-9]){10}$/.test(value);
		}, '');
		var form = $('#feedback form');
		form.validate({
			errorClass: 'error',
			invalidHandler: function (event, validator) {
				//console.log(validator);
			},
			errorPlacement: function (error, element) {
				// error.appendTo( element.closest(''));
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('error').removeClass('valid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('error').addClass('valid');
			},
			rules: {
				name: {
					required: true,
					minlength: 2
				},
				phone: {
					required: true,
					phoneRU: true
				},
				time: {
					required: true
				},
				email: {
					required: true,
					email: true
				}
			}
		});
	}
	$('input[type="number"]').css({
		//    left: 40,
		//    position: 'relative'
	}).after(function () {
		return $('<div />', {
			'class': 'spinner_b',
			//        css : {
			//            height: $(this).outerHeight(),
			//            top: $(this).position().top,
			//            left: $(this).position().left - 40,
			//        },
			//text: '-'
			text: ''
		}).on('click', { input: this }, function (e) {
			e.data.input.value = (+e.data.input.value) - 1;
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_t').removeClass('active');
			$(this).addClass('active');
		});
	}).before(function () {
		return $('<div />', {
			'class': 'spinner_t',
			css: {
				//            height: $(this).outerHeight(),
				//            top: $(this).position().top,
				//            left: $(this).position().left + $(this).width(),
			},
			//text: '+'
			text: ''
		}).on('click', { input: this }, function (e) {
			e.data.input.value = (+e.data.input.value) + 1;
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_b').removeClass('active');
			$(this).addClass('active');
		});
	});

	$('input.number').css({
		//    left: 40,
		//    position: 'relative'
	}).after(function () {
		return $('<div />', {
			'class': 'spinner_b',
			//        css : {
			//            height: $(this).outerHeight(),
			//            top: $(this).position().top,
			//            left: $(this).position().left - 40,
			//        },
			//text: '-'
			text: ''
		}).on('click', { input: this }, function (e) {
			//e.data.input.value = 1;

			if (e.data.input.value < 0) {
				e.data.input.value = (+e.data.input.value) + 1;
			} else if (e.data.input.value > 0) {
				e.data.input.value = (+e.data.input.value) - 1;
			} else if (e.data.input.value = 0) {
				e.data.input.value = 1;
			}
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_t').removeClass('active');
			$(this).addClass('active');
		});
	}).before(function () {
		return $('<div />', {
			'class': 'spinner_t',
			css: {
				//            height: $(this).outerHeight(),
				//            top: $(this).position().top,
				//            left: $(this).position().left + $(this).width(),
			},
			//text: '+'
			text: ''
		}).on('click', { input: this }, function (e) {
			e.data.input.value = (+e.data.input.value) + 1;
			$(this).closest('.input').addClass('input--filled');
			$('.spinner_b').removeClass('active');
			$(this).addClass('active');
		});
	});

	$('input[type="number"]').blur(function () {
		$('.spinner_t, .spinner_b').removeClass('active');
	})



	//	window.dzsscr_init($('body'),{
	//		'type':'scrollTop'
	//		,'settings_skin':'skin_apple'
	//		,enable_easing: 'on'
	//		, settings_autoresizescrollbar: 'on'
	//		,settings_chrome_multiplier : 0.04
	//	})
	masc();
	passport();
	kod();
	snils();
	index_city();

	$(function () {
		$(".datepicker").datepicker({
			language: 'ru',
			dateFormat: 'dd.mm.yy',
			prevText: '',
			nextText: '',
			showAnim: 'slideDown',
			changeYear: true,
			yearRange: "1910:2016",
			onSelect: function () {
				$('#input-5').parent('.input').addClass('input--filled');
				setTimeout(function () {
					$('#ui-datepicker-div').find("select").chosen({
						"disable_search": true,
						width: '100%'
					});
					$('#ui-datepicker-div').find(".chosen-results").niceScroll({
						horizrailenabled: false,
						autohidemode: false,
						cursorfixedheight: 20
					});
				}, 100);
			},
			beforeShow: function (input, inst) {
				setTimeout(function () {
					$('#ui-datepicker-div').find("select").chosen({
						"disable_search": true,
						width: '100%'
					});
					$('#ui-datepicker-div').find(".chosen-results").niceScroll({
						horizrailenabled: false,
						autohidemode: false,
						cursorfixedheight: 20
					});
				}, 100);
			},
			onChangeMonthYear: function () {
				setTimeout(function () {
					$('#ui-datepicker-div').find("select").chosen({
						"disable_search": true,
						width: '100%'
					});
					$('#ui-datepicker-div').find(".chosen-results").niceScroll({
						horizrailenabled: false,
						autohidemode: false,
						cursorfixedheight: 20
					});
				}, 10);
			}
		});
	});

	$('#fond_chosen .chosen-single').on('click', function () {
		if (!$(this).find('.custom_txt').is('.active')) {
			$(this).find('span').attr('data-text', $(this).find('span').text());
			$(this).find('span').text('');
		} else
			if ($(this).closest('.chosen-container').is('.active') && !!!$(this).find('span').text()) {
				if ($(this).find('.custom_txt').is('.active')) {
					$(this).find('span').text($(this).find('span').attr('data-text'));
				}
				$(this).closest('.chosen-container').removeClass('active')
				$(this).find('.custom_txt').removeClass('active');
			}

		$(this).closest('.chosen-container').addClass('active')
		$(this).find('.custom_txt').addClass('active');
		event.stopPropagation();
		$(document).unbind('click').bind('click', function () {
			if ($('#fond_chosen').is('.active') && !!!$('#fond_chosen').find('span').text()) {
				var container = $('#fond_chosen');
				if (container.find('.custom_txt').is('.active')) {
					container.find('span').text(container.find('span').attr('data-text'));
				}
				container.removeClass('active')
				container.find('.custom_txt').removeClass('active');
			}
			$(document).unbind('click');
		});


	});
	setTimeout(function () {
		$('<p class="custom_txt">Выберите фонд</p>').prependTo($("#fond_chosen .chosen-single"));
		$('<p class="custom_txt">Город</p>').insertBefore($("#input_11_chosen .chosen-single"));
		$('<p class="custom_txt">Город</p>').insertBefore($("#input_16_chosen .chosen-single"));
	}, 300);

	//(function ($) {
	//	$(function () {
	$('ul.part_cp').on('click', 'li:not(.active)', function () {
		if ($('#slideCircles').length <= 0) {
			$(this)
				.addClass('active').siblings().removeClass('active')
				.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
		} else {

			var index = parseInt($(this).attr('data-index'));
			var topOfCircles = parseInt($('#slideCircles').offset().top);
			//if (topOfCircles > 700) {
			//	var topval = parseInt($('.tabs__content.tc' + index).parents('div.row').offset().top) - parseInt($('.tab_corporate').outerHeight());
			//	$.scrollTo(topval, 500);
			//}
			//showCirclesAnimation($('.tabs__content.tc' + index));
			SlideToIndex(index);
		}
	});
	$('a.play_animate').click(function () {
		if ($(this).hasClass('pause')) {
			$(this).removeClass('pause')
			breakAnimation = false;
			InitSlideCircles();
		} else {
			breakAnimation = true;
			$(this).addClass('pause')
		}
	});
	//	});
	//})(jQuery);

	//$('.sch1').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt1)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt1').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch1)').addClass('ops');
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch2').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt2)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt2').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch2)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch3').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt3)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt3').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch3)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch4').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt4)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt4').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch4)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch5').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt5)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt5').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch5)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch6').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt6)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt6').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch6)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch7').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt7)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt7').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch7)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch8').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt8)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt8').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch8)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch9').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt9)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt9').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch9)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch10').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt10)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt10').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch10)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch11').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt11)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt11').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch11)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch12').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt12)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt12').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch12)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch13').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt13)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt13').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch13)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});
    //
	//$('.sch14').click(function () {
	//	$('.blc_schedule .blc_all_sch_txt:not(.sch_txt14)').removeClass('active');
	//	$(this).closest('.blc_schedule').find('.sch_txt14').toggleClass('active');
	//	$('.blc_schedule .blc_all_sch:not(.sch14)').addClass('ops');
    //
	//	setTimeout(function () {
	//		if ($('.blc_all_sch_txt').is('.active')) {
	//			$('.blc_all_sch_txt').addClass('hello')
	//		} else {
	//			$('.blc_schedule .blc_all_sch').removeClass('ops');
	//		}
	//	}, 180);
	//	$(this).removeClass('ops');
	//});

	$(function () {



	});

	//	var myWidth = 0;
	//    myWidth = window.innerWidth;
	//    jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:999;color:#fff;">Width = '+myWidth+'</div>');
	//    jQuery(window).resize(function(){
	//        var myWidth = 0;
	//        myWidth = window.innerWidth;
	//        jQuery('#size').remove();
	//        jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:999;color:#fff;">Width = '+myWidth+'</div>');
	//    });
}



function CloseAllContent() {
	$('.ver_tabs li').each(function () {
		CloseContent($(this));
	});
}
function OpenAllContent() {
	$('.ver_tabs li').each(function () {
		OpenContent($(this));
		if ($(this).hasClass('open')) {
			OpenContent($(this));
		}
	});
}

function OpenContent(element) {
	if (!element.find('.direction').hasClass('active')) {
		//$('.hor_tabs li:first-child').addClass('active_plus');
		element.toggleClass('active');
		element.find('.direction').addClass('active');
		element.closest('li').toggleClass('active');
		element.closest('li').find('.desc').slideToggle();
		//$('.hor_tabs li:last-child').toggleClass('active');			
	}
}
function CloseContent(element) {
	if (element.find('.direction').hasClass('active')) {
		element.removeClass('open');
		element.find('.desc').slideUp();
		//$(this).closest('li').find('.desc').slideDown();
		//$('.hor_tabs li:last-child').addClass('active');
		//$('.hor_tabs li:first-child').removeClass('active');
		element.find('.direction').removeClass('active');
		//$(this).addClass('active');	
		//$('.hor_tabs li:first-child').addClass('active_plus');
	}
}

function CheckFiltersPosition() {
	if ($('.blc_fix_fltr').length > 0) {

		var topPosition = $('.blc_fix_fltr').parents('div.row').offset().top;
		if ($('.blc_fix_fltr').attr("data-top") != "0") {
			topPosition = $('.blc_fix_fltr').attr("data-top");
		}
		var topPositionValue = parseInt(topPosition);
		if ($(window).scrollTop() + 120 >= topPositionValue) {
			if ($('.blc_fix_fltr').attr("data-top") == "0") {
				$('.blc_fix_fltr').attr("data-top", $('.blc_fix_fltr').parents('div.row').offset().top);
				//$('.tabs_corp').parent("div").css("margin-top", "120px");
			}
			$('.bg_menu').addClass('not_opst');
			$('.blc_fix_fltr').addClass("fix_fltr");
			$('.blc_fix_fltr').parents('div.blc_nt_fx_cnt').addClass("active");
			$('#slideCircles').css("margin-top", "166px");
		}

		else {
			if ($('.blc_fix_fltr').attr("data-top") != "0") {
				$('.blc_fix_fltr').attr("data-top", "0");
				//$('.tabs_corp').parent("div").css("margin-top", "auto");
			}
			$('.blc_fix_fltr').parents('div.blc_nt_fx_cnt').removeClass("active");
			$('.bg_menu').removeClass('not_opst');
			$('.blc_fix_fltr').removeClass("fix_fltr");
			$('#slideCircles').css("margin-top", "auto");
		}
	}
}

function CheckSubmenuPosition() {
	if ($('#submenu-support').length > 0) {

		var topPosition = $('#submenu-support').offset().top;
		if ($('#submenu-support').attr("data-top") != "0") {
			topPosition = $('#submenu-support').attr("data-top");
		}
		var topPositionValue = parseInt(topPosition);
		if ($(window).scrollTop() + 100 >= topPositionValue) {
			if ($('#submenu-support').attr("data-top") == "0") {
				$('#submenu-support').attr("data-top", $('#submenu-support').offset().top);
				$('#submenu-support').parent("div").css("margin-top", "120px");
			}
			$('#submenu-support').addClass("hor_tabs_fix");
			$('#submenu-support').css("margin-top", "70px");
		}

		else {
			if ($('#submenu-support').attr("data-top") != "0") {
				$('#submenu-support').attr("data-top", "0");
				$('#submenu-support').parent("div").css("margin-top", "auto");
			}
			$('#submenu-support').removeClass("hor_tabs_fix");
			$('#submenu-support').css("margin-top", "auto");
		}
	}
}

function CheckSubmenuPositionForPartner() {
	if ($('.tabs_partners').length > 0) {
		var topPosition = $('.tabs_partners').offset().top;
		if ($('.tabs_partners').attr("data-top") != "0") {
			topPosition = $('.tabs_partners').attr("data-top");
		}
		var topPositionValue = parseInt(topPosition);
		if ($(window).scrollTop() + 100 >= topPositionValue) {
			if ($('.tabs_partners').attr("data-top") == "0") {
				$('.tabs_partners').attr("data-top", $('.tabs_partners').offset().top);
				$('.tabs_partners').parent("div").css("margin-top", "70px");
			}
			$('.tabs_partners').addClass("hor_tabs_fix");
			$('.tabs_partners').css("margin-top", "70px");
		}

		else {
			if ($('.tabs_partners').attr("data-top") != "0") {
				$('.tabs_partners').attr("data-top", "0");
				$('.tabs_partners').parent("div").css("margin-top", "auto");
			}
			$('.tabs_partners').removeClass("hor_tabs_fix");
			$('.tabs_partners').css("margin-top", "auto");
		}
	}
}

function InitPlugins() {
	var listLi = $('ul.all_pack li:first-child ul.charter li, .pensioner_admin_folder_main ul li').length;
	if (listLi > 4) {
		$('ul.all_pack li:first-child ul.charter, .pensioner_admin_folder_main ul').addClass('lot_of');
	} else {
		$('ul.all_pack li:first-child ul.charter, .pensioner_admin_folder_main ul').removeClass('lot_of');
	}

	//$(".fancybox").fancybox();

	$("select:not(#fond)").chosen({
		disable_search_threshold: 1,
		no_results_text: "Результат не найден!",
		//"disable_search": true, //поле поиска выключено
		//"disable_search": true, //поле поиска включено
		width: '100%'
	}).on('chosen:showing_dropdown', function (el, args) {
		args.chosen.container.find('.arw_cls').remove();
		$(args.chosen.search_field).removeAttr('readonly').after('<div class="arw_cls"></div>');
		$(args.chosen.form_field).trigger('chosen:updated');

		if (!args.chosen.container.find('.custom_txt').is('.activecity')) {
			args.chosen.container.find('span').attr('data-text', $(this).find('span').text());
			args.chosen.container.find('span').text('');
			args.chosen.container.addClass('active')
			args.chosen.container.find('.custom_txt').addClass('activecity');
		}

		args.chosen.container.closest('li').find('.arw_cls').off('click.y');
		args.chosen.container.closest('li').find('.arw_cls').on('click.y', function () {
			setTimeout(function () {
				//console.log('close');
				// $(args.chosen.form_field).trigger('chosen:open');
				$(args.chosen.form_field).trigger('chosen:close');
			}, 200);
		});


		//console.log($('.chosen-helper-close'));
		$(args.chosen.form_field).trigger('chosen:updated');
		$(args.chosen.search_results).getNiceScroll().show();
	}).on('chosen:hiding_dropdown', function (el, args) {

		//if (args.chosen.container.is('.active') && !!!args.chosen.container.find('span').text()) {
		var city = args.chosen.container.find('span').attr('data-text');
		if (!!!city) {
			city = args.chosen.container.find('span').text();
		}
		var defaultCity = args.chosen.container.closest('li').find('select').attr('data-placeholder');
		if (!!!args.chosen.container.find('span').text() || city == defaultCity) {
			if (args.chosen.container.find('.custom_txt').is('.activecity')) {

				args.chosen.container.find('span').text($(this).find('span').attr('data-text'));
				args.chosen.container.removeClass('active')
				args.chosen.container.find('.custom_txt').removeClass('activecity');
			}
		}

		$(args.chosen.search_results).getNiceScroll().hide();
	});

	// $("select:not(#fond)").on('chosen:ready',function(){
	// alert('bla');
	// });

	$("select#fond").chosen({
		//"disable_search": true, //поле поиска выключено
		"disable_search": true, //поле поиска включено
		width: '100%'
	});

	//$('.two_li .chosen-search').append('<div class="arw_cls"></div>');
	// $("select").bind("chosen:maxselected",function(){
	// if($(this).find('.custom_txt').is('.active'))
	// {
	// $(this).find('span').text($(this).find('span').attr('data-text'));
	// }
	// $(this).closest('.chosen-container').removeClass('active')
	// $(this).find('.custom_txt').removeClass('active');				
	// });

	$('.chosen-results').niceScroll({
		horizrailenabled: false,
		autohidemode: false,
		cursorfixedheight: 20
	});
	nScroll();
	$("#ascrail2003").appendTo(".txtr");
	$('body').niceScroll({
		touchbehavior: false,
		autohidemode: false,
		cursorborderradius: "100px",
		railoffset: true,
		cursorfixedheight: 150,
		zindex: 100,
		enablescrollonselection: false,
		cursordragontouch: true,
		cursorborder: "0",
		cursorwidth: "5"
	});

	(function ($) {
		$(function () {
			$('ul.part, .ftabs').on('click', 'li:not(.active)', function () {
				$(this)
					.addClass('active').siblings().removeClass('active')
					.closest('div.tabs, .blc_ftabs').find('div.tabs__content, .blc_ftabs_cnt').removeClass('active').eq($(this).index()).addClass('active');
				var index = $(this).attr('data-index');
				var topOfCircles = parseInt($('.number_participants').offset().top);
				if (topOfCircles > 700) {
					var topval = parseInt($('.tabs__content.tc' + index).parents('div.row').offset().top) - parseInt($('.tabs_partners').outerHeight());
					$.scrollTo(topval, 500);
				}
				showCirclesAnimation($('.tabs__content.tc' + index));
			});
		});
	})(jQuery);

}

function nScroll() {
	if ($('div').is('.step3')) {
		$('.step3 textarea').niceScroll({
			touchbehavior: false,
			autohidemode: false,
			cursorborderradius: "100px",
			railoffset: true,
			cursorfixedheight: 20,
			zindex: 100,
			enablescrollonselection: false,
			cursordragontouch: true
		});
	}
}

function masc() {
	$.mask.definitions['d'] = '[0-9]';
	$("#input-13, #input-14, #input-18, #input-19").mask("");
}

function passport() {
	$.mask.definitions['h'] = '[A-Fa-f0-9]';
	$("#input-4").mask("hhhh hhhhhh");
}

function kod() {
	$("#input-6").mask("999 999");
}

function snils() {
	$("#input-7").mask("999 999999 99");
}

function index_city() {
	$("#input-10, #input-15").mask("999999");
}







//$('#input_11_chosen .chosen-single').on('click', function () {
//	$('#input_11_chosen').find('.chosen-drop').removeClass('isactive').addClass('isactive');
//	if (!$(this).find('.custom_txt').is('.active')) {
//		$(this).find('span').attr('data-text', $(this).find('span').text());
//		$(this).find('span').text('');
//
//		//$(this).closest('.chosen-container').find('input').css('background', '');
//		//if ($(this).closest('.chosen-container').find('chose-city').length <= 0) {
//		//	$(this).closest('.chosen-container').find('input').after('<img class="chose-city" style="height: 15px;position: absolute;width: 15px;top: 0;right: 0;cursor: pointer;background-color: black;" src="/upload/img/logo_fix.png"/>');
//		//	$(this).closest('.chosen-container').find('.chose-city').unbind('click').bind('click', function () {
//		//		var city = $(this).closest('.chosen-container').find('input').val();
//		//		$(this).closest('li').find('select option:first').val(city);
//		//		$(this).closest('.chosen-container').find('span').text(city)
//		//		$(this).closest('.chosen-container').removeClass('chosen-with-drop');
//		//		//$(this).closest('li').find('select').trigger("chosen:close");
//		//		setTimeout(function () {
//		//			$("select").trigger("chosen:close");
//		//		}, 600);
//		//	});
//		//}
//
//	} else
//		if ($(this).closest('.chosen-container').is('.active') && !!!$(this).find('span').text()) {
//			if ($(this).find('.custom_txt').is('.active')) {
//				$(this).find('span').text($(this).find('span').attr('data-text'));
//			}
//			$(this).closest('.chosen-container').removeClass('active')
//			$(this).find('.custom_txt').removeClass('active');
//		}
//	$(this).closest('.chosen-container').addClass('active')
//	$(this).find('.custom_txt').addClass('active');
//
//	event.stopPropagation();
//	$(document).unbind('click').bind('click', function () {
//		$('#input_11_chosen').find('.chosen-drop').removeClass('isactive');
//		setTimeout(function () {
//			$('#input_11_chosen').closest('li').find("select").trigger("chosen:close");
//		}, 100);
//		if ($('#input_11_chosen').is('.active') && !!!$('#input_11_chosen').find('span').text()) {
//			var container = $('#input_11_chosen');
//			if (container.find('.custom_txt').is('.active')) {
//				container.find('span').text(container.find('span').attr('data-text'));
//			}
//			container.removeClass('active')
//			container.find('.custom_txt').removeClass('active');
//		}
//		$(document).unbind('click');
//	});
//});	
//$('#input_16_chosen .chosen-single').on('click', function(){
//	if(!$(this).find('.custom_txt').is('.active'))
//	{
//		$(this).find('span').attr('data-text',$(this).find('span').text());
//		$(this).find('span').text('');
//	}else 
//		if($(this).closest('.chosen-container').is('.active')&&!!!$(this).find('span').text())
//		{				
//			if($(this).find('.custom_txt').is('.active'))
//			{
//				$(this).find('span').text($(this).find('span').attr('data-text'));
//			}
//			$(this).closest('.chosen-container').removeClass('active')
//			$(this).find('.custom_txt').removeClass('active');							
//		}
//	$(this).closest('.chosen-container').addClass('active')
//	$(this).find('.custom_txt').addClass('active');

//	event.stopPropagation();
//	$(document).unbind('click').bind('click',function(){
//		if($('#input_16_chosen').is('.active')&&!!!$('#input_16_chosen').find('span').text())
//		{
//			var container=$('#input_16_chosen');
//			if(container.find('.custom_txt').is('.active'))
//			{
//				container.find('span').text(container.find('span').attr('data-text'));
//			}
//			container.removeClass('active')
//			container.find('.custom_txt').removeClass('active');							
//		}
//		$(document).unbind('click');
//	});
//});		





//	var myWidth = 0;
//	myWidth = window.innerWidth;
//	jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	jQuery(window).resize(function(){
//		var myWidth = 0;
//		myWidth = window.innerWidth;
//		jQuery('#size').remove();
//		jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	});	



//$('[data-required="step1"]')
function InitFeedbackSteps() {
	var feeledPropertiesStep2 = 0;
	$('[data-required="step2"]').each(function () {
		if (!!$(this).val()) {
			feeledPropertiesStep2++;
		}
	});
	HideStep($('div.step3'));

	HideStep($('div.step2_2'));

	if (feeledPropertiesStep2 > 0) {
		ShowStep2();
	} else {
		HideStep($('div.step2'));
	}

	$('[data-required="step1"]').on("keyup", function () {
		if (CheckStep1Feeling()) {
			AnimatedShowStep($('div.step2'));
		} else {
			AnimatedHideStep($('div.step2'));
		}
	});

	$('#radio1').click(function () {
		if ($('#radio2').is(":checked") && $('#radio4').is(":checked")) {
			AnimatedShowStep($('div.step2_2'));
		} else {
			AnimatedHideStep($('div.step2_2'));
		}
	});
	$('#radio2').click(function () {
		if ($(this).is(":checked") && $('#radio4').is(":checked")) {
			AnimatedShowStep($('div.step2_2'));
		} else {
			AnimatedHideStep($('div.step2_2'));
		}
	});

	$('#radio3').click(function () {
		if ($('#radio4').is(":checked") && $('#radio2').is(":checked")) {
			AnimatedShowStep($('div.step2_2'));
		} else {
			AnimatedHideStep($('div.step2_2'));
		}
	});
	$('#radio4').click(function () {
		if ($(this).is(":checked") && $('#radio2').is(":checked")) {
			AnimatedShowStep($('div.step2_2'));
		} else {
			AnimatedHideStep($('div.step2_2'));
		}
	});

	$('[data-required="step2"]').on("keyup", function () {
		if (CheckStep2Feeling($('div.step3'))) {
			AnimatedShowStep($('div.step3'));
		}
	});
}

function CheckStep1Feeling() {
	var feeledPropertiesStep1 = 0;
	var countRequiredElements = $('[data-required="step1"]').length;
	$('[data-required="step1"]').each(function () {
		if (!!$(this).val()) {
			feeledPropertiesStep1++;
		}
	});
	if (feeledPropertiesStep1 == countRequiredElements) {
		return true;
	}
	return false;
}

function CheckStep2Feeling() {
	var feeledPropertiesStep2 = 0;
	var countRequiredElements = $('[data-required="step2"]').length;
	$('[data-required="step2"]').each(function () {
		if (!!$(this).val()) {
			feeledPropertiesStep2++;
		}
	});
	if (feeledPropertiesStep2 == countRequiredElements - 2) {
		return true;
	}
	return false;
}

function ShowStep2() {
	var height = $('div.step2').attr('data-height');
	if (!!height) {
		$('div.step2').css('display', 'block').css('height', height);
	}
}

function AnimatedShowStep(element) {
	var height = element.attr('data-height');
	if (!!height) {
		element.css('display', 'block');
		element.stop().animate({ 'height': height }, {
			easing: "linear",
			duration: 800
		});
	}
}

function AnimatedHideStep2() {
	var height = $('div.step2').attr('data-height');
	if (!!!height) {
		height = $('div.step2').height();
	}
	$('div.step2').attr('data-height', height);
	$('div.step2').stop().animate({ 'height': 0 }, {
		easing: "linear",
		duration: 800,
		complete: function () {
			$('div.step2').css('display', 'none');
		}
	});
}

function AnimatedHideStep(element) {
	var height = element.attr('data-height');
	if (!!!height) {
		height = element.height();
	}
	element.attr('data-height', height);
	element.stop().animate({ 'height': 0 }, {
		easing: "linear",
		duration: 800,
		complete: function () {
			element.css('display', 'none');
		}
	});
}

function HideStep(element) {
	var height = element.attr('data-height');
	if (!!!height) {
		height = element.height();
	}
	element.attr('data-height', parseInt(height));
	element.css('display', 'none').css('height', '0');
}