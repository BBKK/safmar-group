$(document).ready(function () {});

var slr_main = new Swiper('.swiper_slr', {
	autoplay: 1700,
	speed: 2200,
	slidesPerView: 3,
	loop: true,
	spaceBetween: 0,
	effect: 'slide',
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',	
    observer:true,
    observeParents:true,	
	  breakpoints: {
    // when window width is <= 320px
    384: {
      slidesPerView: 1,
      spaceBetween: 0
    },
    // when window width is <= 480px
    580: {
      slidesPerView: 2,
      spaceBetween: 0
    },
    // when window width is <= 640px
    767: {
      slidesPerView: 3,
      spaceBetween: 0
	},
    1200: {
      slidesPerView: 4,
      spaceBetween: 0
    }		  
  }
});


var slr_foto = new Swiper('.swiper_foto', {
	autoplay: 1700,
	speed: 5200,
	slidesPerView: 3,
	loop: true,
	spaceBetween: 0,
	effect: 'slide',
    observer:true,
    observeParents:true,
    autoResize: true,
    DOMAnimation: true,
    preventLinks: true,
	grabCursor: true,
	breakpoints: {
    // when window width is <= 320px
    650: {
      slidesPerView: 1,
      spaceBetween: 0
	},
	991: {
      slidesPerView: 2,
      spaceBetween: 0
	}
  }
});

var slr_foto2 = new Swiper('.swiper_foto2', {
	autoplay: 0,
	speed: 5200,
	slidesPerView: 3,
	loop: true,
	spaceBetween: 0,
	effect: 'slide',
    observer:true,
    observeParents:true,
    autoResize: true,
    DOMAnimation: true,
    preventLinks: true,
	grabCursor: true,
	  breakpoints: {
    // when window width is <= 320px
    650: {
      slidesPerView: 1,
      spaceBetween: 0
	},
	991: {
      slidesPerView: 2,
      spaceBetween: 0
	}
  }
});