<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
global $month_name, $month_content, $news_content, $all_content, $paginator_content, $arrFilter;
if(empty($_POST['count'])){
        $count = 10000;
}else{
        $count = $_POST['count'];
}
if($_POST['id'] == 0){
        $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.01.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.12.'.$_POST['year'].' 23:59:59');
        
}else{
        if($_POST['id'] == 1){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.01.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.03.'.$_POST['year'].' 23:59:59');
        }elseif($_POST['id'] == 2){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.04.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'30.06.'.$_POST['year'].' 23:59:59');
        }elseif($_POST['id'] == 3){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.07.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.09.'.$_POST['year'].' 23:59:59');
        }elseif($_POST['id'] == 4){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.10.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.12.'.$_POST['year'].' 23:59:59');
        }

}

?>

<?$APPLICATION->IncludeComponent(
        "bitrix:news.list", 
        "news_ajax", 
        array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "CONTENT",
                "IBLOCK_ID" => "6",
                "NEWS_COUNT" => $count,
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC", 
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array(
                        0 => "ID",
                        1 => "CODE",
                        2 => "NAME",
                        3 => "SORT",
                        4 => "PREVIEW_TEXT",
                        5 => "PREVIEW_PICTURE",
                        6 => "DETAIL_TEXT",
                        7 => "DETAIL_PICTURE",
                        8 => "DATE_ACTIVE_FROM",
                        9 => "IBLOCK_TYPE_ID",
                        10 => "IBLOCK_ID",
                        11 => "DATE_CREATE",
                        12 => "",
                ),
                "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_NOTES" => "",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PAGER_TEMPLATE" => "page",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
        ),
        false
);?>

<?

$result['text'] = $news_content;

if(!empty($result['text'])){
        $result['type'] = true;
}else{
        $result['type'] = false;
}
echo json_encode($result);