<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
global $month_name, $month_content, $news_content, $all_content, $pref, $next;
/*if(empty($_POST['count'])){
        $count = 10000;
}else{
        $count = $_POST['count'];
}
if($_POST['id'] == 0){
        $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.01.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.12.'.$_POST['year'].' 23:59:59');
        
}else{
        if($_POST['id'] == 1){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.01.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.03.'.$_POST['year'].' 23:59:59');
        }elseif($_POST['id'] == 2){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.04.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'30.06.'.$_POST['year'].' 23:59:59');
        }elseif($_POST['id'] == 3){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.07.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.09.'.$_POST['year'].' 23:59:59');
        }elseif($_POST['id'] == 4){
                $arrFilter = array(">=DATE_ACTIVE_FROM"=>'01.10.'.$_POST['year'], "<=DATE_ACTIVE_FROM"=>'31.12.'.$_POST['year'].' 23:59:59');
        }

}*/

?>
<?$APPLICATION->IncludeComponent("bitrix:news.detail", "news_detail", Array(   
        "IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => "6",	// Код информационного блока
        "ELEMENT_ID" => "",	// ID новости
        "ELEMENT_CODE" => $_POST['CODE'],	// Код новости
        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        "FIELD_CODE" => array(	// Поля
                0 => "",
                1 => "undefined",
                2 => "",
        ),
        "PROPERTY_CODE" => array(	// Свойства
                0 => "",
                1 => "undefined",
                2 => "",
        ),
        "IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
        "AJAX_MODE" => "N",	// Включить режим AJAX
        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        "CACHE_TYPE" => "N",	// Тип кеширования
        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
        "SET_BROWSER_TITLE" => "Y",
        "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
        "SET_META_KEYWORDS" => "Y",
        "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
        "SET_META_DESCRIPTION" => "Y",
        "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
        "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
        "ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
        "DISPLAY_NAME" => "Y",	// Выводить название элемента
        "DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
        "USE_SHARE" => "N",	// Отображать панель соц. закладок
        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
        "PAGER_TITLE" => "Страница",	// Название категорий
        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        ),
        false
);?>
                        
<?

$result['text'] = $news_content;
$result['next'] = $next['CODE'];
$result['pref'] = $pref['CODE'];
if(!empty($result['text'])){
        $result['type'] = true;
}else{
        $result['type'] = false;
}
echo json_encode($result);