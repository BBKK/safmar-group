	<div class="bg_gray_btm">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<ul class="mst">
						<li>
							<ul>
								<li><h4>О группе</h4></li>
								<li><a href="/osnovateli/">основатели</a></li>
								<li><a href="/istoriya/">история</a></li>
								<li><a href="/korporativnoe-upravlenie/">корпоративное управление</a></li>
								<li><a href="/blagotvoritelnost/">благотворительность</a></li>
							</ul>
						</li>
						<li>
							<ul>
								<li><h4>Активы</h4></li>
								<li><a href="#">банки</a></li>
								<li><a href="#">пенсионные фонды</a></li>
								<li><a href="#">лизинг</a></li>
								<li><a href="#">страхование</a></li>
								<li><a href="#">нефинансовые активы</a></li>
							</ul>
						</li>
						<li>
							<ul>
								<li><h4>Контакты</h4></li>
								<li><a href="/contact/">фактический адрес</a></li>
								<li><a href="tel:88002223344">8 800 222 33 44</a></li>
								<li><a href="mailto:info@safmargroup.ru">info@safmargroup.ru</a></li>
							</ul>
						</li>
						<li>
							<ul>
								<li><h4>Новости</h4></li>
								<li><a href="/news/">новости</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="blc_gb_link_btm">
						<a href="/raskrytie-informatsii/">Раскрытие информации по АО «ФГ САФМАР»</a>
						<a href="/ogranichenie-otvetstvennosti/">Ограничение ответственности</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="blc_safmar">
						<a href="#"><img src="/html/images/flogo.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="footer_logos">
				<div class="slr_our_clients">
					<div class="swiper_slr swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<a href="#"><img src="/html/images/slr/1.png" alt=""></a>
								<a href="#"><img src="/html/images/slr/2.png" alt=""></a>
							</div>
							<div class="swiper-slide">
								<a href="#"><img src="/html/images/slr/3.png" alt=""></a>
								<a href="#"><img src="/html/images/slr/4.png" alt=""></a>
							</div>
							<div class="swiper-slide">
								<a href="#"><img src="/html/images/slr/5.png" alt=""></a>
								<a href="#"><img src="/html/images/slr/6.png" alt=""></a>
							</div>
							<div class="swiper-slide">
								<a href="#"><img src="/html/images/slr/7.png" alt=""></a>
								<a href="#"><img src="/html/images/slr/1.png" alt=""></a>
							</div>
						</div>
					</div>
				</div><!--slr_our_clients-->
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>				
			</div>
		</div>
		<div class="bg_ftr"></div>
	</div><!--footer-->
	<script src="/html/js/swiper/swiper.min.js"></script>
	<script src="/html/js/swiper/slr.js"></script>