	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#00c969">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<link rel="shortcut icon" href="/html/images/favicon.ico">
	<title>Сафмар </title>
	<link rel="stylesheet" href="/html/css/jquery-ui.min.css" type="text/css"/>
	<link rel="stylesheet" href="/html/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/html/css/fancybox/jquery.fancybox.css">
	<link rel="stylesheet" href="/html/css/chosen/chosen.css">
	<link rel="stylesheet" href="/html/css/swiper/swiper.min.css">
	<link rel="stylesheet" href="/html/css/styles.css">
	<link rel="stylesheet" href="/html/css/dzsparallaxer.css">
	<script src="/html/js/jquery.js"></script>
	<script src="/html/js/jquery-ui.js"></script>
        <script src="/html/js/news.js"></script>
	<script src="/html/bootstrap/js/bootstrap.min.js"></script>
	<script src="/html/js/chosen.jquery.min.js"></script>
	<script src="/html/js/jquery.fancybox.pack.js"></script>
	<script src="/html/js/jquery.scrollTo.min.js"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>
	<script type="text/javascript" src="/html/js/map_contact.js"></script>
	<script src="/html/js/jquery.nicescroll.min.js"></script>
	<script src="/html/js/jquery.maskedinput.js"></script>
	<script src="/html/js/jquery.validate.js"></script>
	<script src="/html/js/datepicker_ru.js"></script>
	<script src="/html/js/circles.min.js"></script>
	<script src="/html/js/action.js"></script>
	<script src="/html/js/dzsparallaxer.js"></script>