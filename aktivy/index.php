<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Активы");
?>
        <div id="header" data-options='{ direction: "normal"}'>
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg4_1.jpg');height: 100%"></div>
		<div class="blc_desc_foto blc_desc_fotoW">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Алексей Саламатоа</strong>, Лебединое озеро</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div><!--container-->		
		</div>		
	</div>
	<!--<div id="header" class="bg_active"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i"></a>
				</div>
			</div>
		</div>
	</div><!--container-->

	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper">

		<div class="cnt_in cnt_persone">
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "activy_group", Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
				"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"DISPLAY_DATE" => "N",	// Выводить дату элемента
				"DISPLAY_NAME" => "Y",	// Выводить название элемента
				"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
				"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"FIELD_CODE" => "",	// Поля
				"FILTER_NAME" => "",	// Фильтр
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
				"IBLOCK_ID" => "5",	// Код информационного блока
				"IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"NEWS_COUNT" => "5",	// Количество новостей на странице
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Новости",	// Название категорий
				"PARENT_SECTION" => "",	// ID раздела
				"PARENT_SECTION_CODE" => "",	// Код раздела
				"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
				"PROPERTY_CODE" => "",	// Свойства
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
				"SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
				"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
				"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
			), false );?>
			<!-- <div class="tabs tab_corporate">
				<div class="container">

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="blc_active_group_ttl">
								<h2>Активы группы</h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="tabs_corp">
								<ul class="part_cp">
									<li class="active"><h5>Банки</h5></li>
									<li><h5>Пенсионные фонды</h5></li>
									<li>
										<h5>Лизинг</h5>
									</li>
									<li>
										<h5>Страхование</h5>
									</li>
									<li>
										<h5>Нефинансовые активы</h5>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div><!--container
				<div class="row mp">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm">
						<div class="tabs__content tc1 active"  data-index="1">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Банковская группа БИНБАНКа</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Одна из крупнейших частных банковских групп России с акцентом на корпоративном и розничном бизнесе и широкой филиальной сетью. Группа динамично развивается как на основе внутренних ресурсов, так и посредством сделок по слиянию и поглощению. </p>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>
											СПо объему активов на 01.01.2016 г. Банковская группа БИНБАНКа занимает 3-е место среди российских частных банковских групп <sup>1</sup>. <sup>1</sup> По данным из обзора Fitch Russian Banks Datawatch (12M 2015) (за 12 мес. 2015 г.)
										</p>
									</div>
								</div>
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="number_participants">
											<li>
												<div class="desc" id="circle11" >
													<div class="circle" id="pcircles-11" data-text="Точек продаж" data-mesure="шт" data-value="550" data-postfix=">"></div>
												</div>
											</li>
											<li>
												<div class="desc" id="circle21" >
													<div class="circle" id="pcircles-12" data-text="Количество сотрудников" data-mesure="чел" data-value="15000" data-postfix=">"></div>
												</div>									
											</li>
											<li>
												<div class="desc" id="circle31" >
													<div class=" circle" id="pcircles-13" data-text="Клиентов физических<br>лиц" data-mesure="чел" data-value="2000000" data-postfix="<br>>"></div>
													<div class="circle" id="pcircles-14" data-text="Клиентов юридических<br>лиц" data-mesure="чел" data-value="85000" data-postfix="<br>>"></div>
												</div>									
											</li>
										</ul>
									</div>
								</div>

								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Совокупные показатели</h6>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Совокупные показатели Банковской Группы на 01.01.2016г.:</p>
										<p>
											<sup>2</sup> Более 3 250 собственных устройств плюс сеть партнеров и агентов в рамках взаимного сотрудничества
										</p>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table>
											<tr>
												<td>Общие активы</td>
												<td>> 1 трл. руб. </td>
											</tr>
											<tr>
												<td>Средства клиентов</td>
												<td>> 0,7 трл. руб.</td>
											</tr>
											<tr>
												<td>Количество клиентов физических лиц</td>
												<td>> 2 млн.</td>
											</tr>
											<tr>
												<td>Количество клиентов юридических лиц</td>
												<td>> 85 тысяч</td>
											</tr>
											<tr>
												<td>Количество сотрудников</td>
												<td>> 15 тысяч</td>
											</tr>
											<tr>
												<td>Количество точек продаж</td>
												<td>> 550</td>
											</tr>
											<tr>
												<td>Количество банкоматов <sup>2</sup></td>
												<td>> 14 тысяч</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Ренкинги Банковской Группы на 01.01.2016г.:</h6>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>
											<sup>3</sup> По данным из обзора Fitch Russian Banks Datawatch (12M 2015) (за 12 мес. 2015 г.)
										</p>
									</div>
								</div>

								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table class="two_table">
											<tr>
												<td></td>
												<td colspan="2">ренкинг / позиция <sup>3</sup></td>
											</tr>
											<tr>
												<td></td>
												<td>среди частных Российских баннков</td>
												<td>среди всех Российских баннков</td>
											</tr>
											<tr>
												<td>Общие активы</td>
												<td>#3</td>
												<td>#6</td>
											</tr>
											<tr>
												<td>Розничные депозиты</td>
												<td>#3</td>
												<td>#7</td>
											</tr>
											<tr>
												<td>Корпоративные кредиты</td>
												<td>#4</td>
												<td>#8</td>
											</tr>
											<tr>
												<td>Розничные кредиты</td>
												<td>#6</td>
												<td>#10</td>
											</tr>
											<tr>
												<td>Корпоративные депозиты</td>
												<td>#7</td>
												<td>#12</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<h5>Состав банковской группы</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="logo_list">
											<li>
												<a href="#">
													<div class="lg1"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg2"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg3"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg4"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg5"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg6"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg7"></div>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="tabs__content tc2" data-index="2">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Группа негосударственных пенсионных фондов</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Фонды группы существляют обязательное пенсионное страхование (ОПС) и негосударственное пенсионное обеспечение (НПО), в том числе досрочное негосударственное пенсионное обеспечение. Все НПФ Группы получили в 2015 году положительное заключение Банка России о соответствии требованиям к участию в системе гарантирования прав застрахованных лиц Государственная корпорация «Агентство</p>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p> по страхованию вкладов» (АСВ) и включены в реестр негосударственных пенсионных фондов – участников системы гарантирования прав застрахованных лиц. Реестр негосударственных пенсионных фондов – участников системы гарантирования прав застрахованных лиц размещен на сайте АСВ в разделе «Гарантирование пенсий» (http://asv.org.ru/pension/)</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="number_participants">
											<li>
												<div class="desc" id="circle12">
													<div class="circle" id="pcircles-21" data-text="Точек продаж" data-mesure="шт" data-value="10" data-postfix=">"></div>
												</div>
											</li>
											<li>
												<div class="desc" id="circle22">
													<div class="circle" id="pcircles-22" data-text="Количество сотрудников" data-mesure="чел" data-value="134" data-postfix=">"></div>
												</div>
											</li>
											<li>
												<div class="desc" id="circle32">
													<div class=" circle" id="pcircles-23" data-text="Клиентов физических<br>лиц" data-mesure="чел" data-value="2000000" data-postfix="<br>>"></div>
													<div class="circle" id="pcircles-24" data-text="Клиентов юридических<br>лиц" data-mesure="чел" data-value="88000" data-postfix="<br>>"></div>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Совокупные показатели</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table>
											<tr>
												<td>Услуги предоставляются в городах России</td>
												<td>> 80 городов </td>
											</tr>
											<tr>
												<td>Количество сотрудников</td>
												<td>> 900</td>
											</tr>
											<tr>
												<td>Застрахованных лиц</td>
												<td>> 2 млн. (8.4% от общего количества по всем НПФ РФ)</td>
											</tr>
											<tr>
												<td>Участников</td>
												<td>> 88 тысяч</td>
											</tr>
											<tr>
												<td>Корпоративных клиентов</td>
												<td>> 260</td>
											</tr>
											<tr>
												<td>Сумма пенсионных накоплений</td>
												<td>> 134 млрд.руб. (7.9% от общей суммы по всем НПФ РФ)</td>
											</tr>
											<tr>
												<td>Сумма пенсионных резервов</td>
												<td>> 10 млрд.руб.</td>
											</tr>
											<tr>
												<td>За 2015 год привлечено новых частных клиентов</td>
												<td>>1,8 млн.</td>
											</tr>
											<tr>
												<td>Доходность инвестирования средств пенсионных накопленийс начала 2016 года</td>
												<td>12.78 %</td>
											</tr>
											<tr>
												<td>Доходность инвестирования средств пенсионных резервовс начала 2016 года</td>
												<td>11.26 %</td>
											</tr>
											<tr>
												<td>Средний счет застрахованного лица</td>
												<td>> 60 тысяч руб.</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<h5>Состав группы негосударственных пенсионных фондов</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="logo_list">
											<li>
												<a href="#">
													<div class="lg"></div>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="tabs__content tc3" data-index="3">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Копания Европлан</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Европлан — крупнейшая автолизинговая негосударственная компания с долей рынка 17.3%.</p>
										<p>С 2007 года занимает 1 место в сегменте автолизинга (лизинг транспорта и самоходной техники) среди негосударственных лизинговых компаний России (по данным ежегодных рейтингов РА «Эксперт» и «Лизинг ревю»).</p>
										<p>В 2013 году Европлан запустил принципиально новый, уникальный для российского рынка продукт — лизинг автомобилей для физических лиц.</p>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>В 2015 году Европлан признан «Лучшей лизинговой компанией России» ((International Finance Magazine Awards).</p>
										<p>Европлан в течение многих лет прочно удерживает за собой лидерство в автолизинге среди негосударственных лизинговых компаний, входит в ТОП-20 автолизинговых компаний в Европе (по данным Leaseurope).</p>
										<p>Европлан предоставляет страховые услуги клиентам по лизингу, тем самым создавая «единое автофинансовое окно» для клиентов. </p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="number_participants">
											<li>
												<div class="desc" id="circle13">
													<div class="circle" id="pcircles-32" data-text="Действующих клиентов" data-mesure="шт" data-value="17000" data-postfix=""></div>
													<div class="circle" id="pcircles-31" data-text="Клиентов по<br>сделкам" data-mesure="шт" data-value="57000" data-postfix=""></div>
												</div>
											</li>
											<li>
												<div class="desc" id="circle23">
													<div class="circle" id="pcircles-33" data-text="Совершонных сделок" data-mesure="шт<br>" data-value="200000" data-postfix=">"></div>
												</div>
											</li>
											<li>
												<div class="desc" id="circle33">
													<div class="circle" id="pcircles-34" data-text="Инвистиции в<br>лизинг" data-mesure="млрд руб" data-value="26.9" data-postfix="<br>>"></div>
													<div class=" circle" id="pcircles-35" data-text="Активы" data-mesure="млрд руб" data-value="42.4" data-postfix=""></div>

												</div>
											</li>
										</ul>
									</div>
								</div>									
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Совокупные показатели</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table>
											<tr>
												<td>Рейтинг Национального Рейтингового Агентства</td>
												<td>А++</td>
											</tr>
											<tr>
												<td>Рейтинг Fitch Ratings</td>
												<td>ВВ</td>
											</tr>
											<tr>
												<td>Доходность инвестирования средств пенсионных накопленийс начала 2016 года</td>
												<td>АА- (rus) рейтинг по национальной шкале</td>
											</tr>
											<tr>
												<td>Лизинговых сделок для 57 000 клиентов за 16 лет работы</td>
												<td>> 200 000</td>
											</tr>
											<tr>
												<td>Действующих клиентов</td>
												<td>> 17 000</td>
											</tr>
											<tr>
												<td>Активы компании</td>
												<td>42,4 млрд.руб.</td>
											</tr>
											<tr>
												<td>Чистые инвестиции в лизинг</td>
												<td>26,9 млрд.руб.</td>
											</tr>
											<tr>
												<td>Капитал компании</td>
												<td>10,6 млрд.руб.</td>
											</tr>
											<tr>
												<td>Чистая прибыль компании за 9 месяцев 2015 года</td>
												<td>1,6 млрд.руб.</td>
											</tr>
											<tr>
												<td>Количество филиалов</td>
												<td>75</td>
											</tr>
											<tr>
												<td>Количество городов присутствия</td>
												<td>72</td>
											</tr>
											<tr>
												<td>Количество сотрудников</td>
												<td>> 2 000</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<h5>Официальный сайт компании</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="logo_list">
											<li>
												<a href="#">
													<div class="lg"></div>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="tabs__content tc4" data-index="4">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Копания БИН Страхование</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Универсальная страховая компания, работающая на рынке с августа 2000г.</p>
										<p>Компания осуществляет страховую защиту банковских клиентов Группы, предоставляя услуги по следующим основным направлениям:</p>
										<ul class="l_list">
											<li>- страхование от несчастных случаев</li>
											<li>- страхование имущества</li>
											<li>- страхование держателей карт</li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Также в линейке «БИН Страхования» присутствует более 50 страховых продуктов для физических и юридических лиц различных отраслей (автомобили, имущество, здоровье, путешествия, транспорт, грузы и прочее).</p>
										<p>Компания представлена в 26 городах Российской Федерации.</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<h6>Страховые сборы</h6>
										<p>Сборы за 2015 г. составили 2 млрд руб. со следующим распределением по видам страхования</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="blc_cut_circle">
											<div class="cr1"></div>
											<div class="cr2"></div>
											<div class="cr3"></div>
											<div class="cr4"></div>
											<div class="cr5"></div>
											<div class="cr6"></div>
											<div class="cr7"></div>
											<div class="cr8"></div>
											<div class="statistic st1">
												<p>финансовые риски</p>
												<span>11%</span>
											</div>
											<div class="statistic st2">
												<p>добровольное медицинское страхование</p>
												<span>12%</span>
											</div>
											<div class="statistic st3">
												<p>ОСАГО</p>
												<span>13%</span>
											</div>
											<div class="statistic st4">
												<p>имущество юридических лиц</p>
												<span>16%</span>
											</div>
											<div class="statistic st5">
												<p>несчастные случаи и болезни</p>
												<span>17%</span>
											</div>
											<div class="statistic st6">
												<p>ответственность</p>
												<span>17%</span>
											</div>
										</div>
									</div>
								</div>


							</div>
						</div>
						<div class="tabs__content tc5" data-index="5">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Группа компаний «ИНТЕКО»</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Один из ведущих российских девелоперов, признанный эксперт отрасли в сфере урбанистики. Специализируется на комплексном освоении территорий в целях жилищного строительства в Москве и регионах России.</p>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Осуществляет полный цикл работ от создания концепции, градостроительного и архитектурного проектирования, возведения объектов до их продажи и последующей эксплуатации.</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Совокупные показатели ИНТЕКО</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table>
											<tr>
												<td>Построенных зданий и комплексов</td>
												<td>> 50</td>
											</tr>
											<tr>
												<td>Площадь построенных сооружений</td>
												<td>1,6 млн м2</td>
											</tr>
											<tr>
												<td>Инвестиционный портфель</td>
												<td>3,9 млн. м2 недвижимости</td>
											</tr>
											<tr>
												<td>Введено в эксплуатацию в 2015 году</td>
												<td>257 тыс. м2</td>
											</tr>
											<tr>
												<td>Планируется ввести в эксплуатацию в 2016 году</td>
												<td>500 тыс. м2</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>ГПАО «Моспромстрой»</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Один из крупнейших участников рынка строительства и недвижимости Москвы, обладающий дифференцированным бизнесом и сильной позицией на рынке присутствия.</p>
										<p>Со дня основания компания специализировалась на строительстве промышленных и административных объектов Москвы.</p>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<p>Специалистами компании были построены и реконструированы самые знаковые здания и сооружения столицы, ставшие ее визитной карточкой. В рамках гостиничного бизнеса Моспромстрой осуществляет управление 9 гостиницами категории 4-5* в Москве, Астане и Минске. Совокупный номерной фонд гостиниц составляет более 2,4 тысячи номеров.</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>Совокупные показатели ГПАО «Моспромстрой»</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table>
											<tr>
												<td>Построенных зданий и комплексов</td>
												<td>> 2000</td>
											</tr>
											<tr>
												<td>Площадь построенных сооружений</td>
												<td>> 2,1 млн м2</td>
											</tr>
											<tr>
												<td>Количество сотрудников</td>
												<td>> 10 тысяч человек</td>
											</tr>
											<tr>
												<td>Количество филиалов</td>
												<td>15</td>
											</tr>
											<tr>
												<td>Количество дочерних обществ</td>
												<td>35</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<h6>АО «А101 ДЕВЕЛОПМЕНТ»</h6>
										<p>АО «А101 ДЕВЕЛОПМЕНТ» специализируется на строительстве объектов недвижимости и реализует крупнейший в России градостроительный Проект «А101» в Новой Москве, представляющий собой объединение разных по архитектурному исполнению жилых районов с единой концепцией «лучшее со всего мира».</p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>АО «А101 ДЕВЕЛОПМЕНТ»</h6>
									</div>
								</div>
								<div class="row pdng">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<table>
											<tr>
												<td>Построенных квартир</td>
												<td>> 40 тыс. </td>
											</tr>
											<tr>
												<td>Площадь построенных сооружений</td>
												<td>> 1 млн м2</td>
											</tr>
											<tr>
												<td>В ближайшие 20 лет будет построено</td>
												<td>до 20 млн.м2 жилой и коммерческой недвижимости</td>
											</tr>
											<tr>
												<td>Площадь земельного банка</td>
												<td>> 2500 га земли на территории Новой Москвы</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<ul class="logo_list">
											<li>
												<a href="#">
													<div class="lg"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg1"></div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="lg2"></div>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->


		</div>

	</div><!--wrapper-->

	<div class="bg_gray_btm">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<ul class="mst">
						<li>
							<ul>
								<li><h4>О группе</h4></li>
								<li><a href="#">основатели</a></li>
								<li><a href="#">история</a></li>
								<li><a href="#">финансовые показатели</a></li>
								<li><a href="#">корпоративное управление</a></li>
								<li><a href="#">миссия и ценности</a></li>
								<li><a href="#">благотворительность</a></li>
							</ul>
						</li>
						<li>
							<ul>
								<li><h4>Активы</h4></li>
								<li><a href="#">банки</a></li>
								<li><a href="#">пенсионные фонды</a></li>
								<li><a href="#">лизинг</a></li>
								<li><a href="#">страхование</a></li>
								<li><a href="#">нефинансовые активы</a></li>
							</ul>
						</li>
						<li>
							<ul>
								<li><h4>Контакты</h4></li>
								<li><a href="#">фактический адрес</a></li>
								<li><a href="tel:88002223344">8 800 222 33 44</a></li>
								<li><a href="mailto:info@safmargroup.ru">info@safmargroup.ru</a></li>
							</ul>
						</li>
						<li>
							<ul>
								<li><h4>Новости</h4></li>
								<li><a href="#">новости</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="blc_gb_link_btm">
						<a href="#">Раскрытие информации по АО «ФГ САФМАР»</a>
						<a href="#">Ограничение ответственности</a>
					</div>
				</div>
			</div>
		</div>
	</div>        
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>