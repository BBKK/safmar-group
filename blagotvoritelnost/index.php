<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("благотворительность");
?>
	<div id="header" data-options='{ direction: "normal"}' class="">
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg3_1_11.jpg');height: 100%"></div>
		<div class="blc_desc_foto">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Максим Панков</strong>, Золотая волна. Вечер на хребте Аибга. Сочинский национальный парк</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div><!--container-->		
		</div>		
	</div>

	<!--<div id="header" class="bg_main"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i btn_iW"></a>
				</div>
			</div>
		</div>
	</div><!--container-->


	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?> 
						</div><!--/.nav-collapse -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper">

		<div class="pensioner_admin">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h2>Благотворительность</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Благотворительный фонд САФМАР</h6>
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<p>
							Некоммерческая благотворительная организация «Благотворительный фонд «САФМАР» была создана 16 сентября 2013 г.
						</p>
						<p>Фонд зарегистрирован Главным управлением Министерства юстиции Российской Федерации по г. Москве за учетным номером № 7714014272.  Учредителем Фонда является АО НК «РуссНефть».</p>
						<p>Председатель Совета Благотворительного фонда «САФМАР» – Михаил Сафарбекович Гуцериев. Директор фонда – Калачева Лариса Юрьевна. </p>
						<p>Поддержку деятельности фонда оказывают АО НК «РуссНефть», ОАО «Нефтиса», ОАО «Русский уголь», ЗАО «Интеко», ПАО «Моспромстрой», ПАО «БИНБАНК», ЗАО «Фортеинвест», ЗАО «Электроснабсбыт», ПАО «Авгур-эстейт», АО «А 101 Девелопмент».</p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<p>Благотворительный Фонд «САФМАР» учрежден для реализации социально значимых долгосрочных проектов в области культуры, искусства, образования, духовного просвещения. Программы Фонда соответствуют критериям прозрачности, четкости стратегии, последовательности в реализации. </p>
						<p>
							Контроль за деятельностью Фонда осуществляют Совет Фонда, Попечительский Совет и Ревизионная Комиссия.
							Благотворительный фонд «САФМАР» входит в состав некоммерческого партнерства грантодающих организаций «Форум Доноров», объединяющего крупнейшие благотворительные фонды России. Члены Форума доноров участвуют в развитии профессионального благотворительного сообщества в России, продвижении лучших практик в сфере филантропии.
						</p>
					</div>
				</div>
				<div class="blc_charity_grf">
					<div class="tabs__content tc5 active" data-index="5">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<ul class="number_participants">
									<li>
										<div class="desc" id="circle15">
											<div class="circle" id="pcircles-51" data-text="Колличество нарпавленией благотворительной деятельности" data-mesure="шт <br>" data-value="9" data-postfix=""></div>
										</div>
									</li>
									<li>
										<div class="desc" id="circle25">
											<div class="circle" id="pcircles-52" data-text="Целевых программ" data-mesure="шт" data-value="37" data-postfix=""></div>
											<div class=" circle" id="pcircles-53" data-text="Охват деятельности в регионах" data-mesure="шт <br>" data-value="11" data-postfix=""></div>
										</div>
									</li>
									<li>
										<div class="desc" id="circle35">
											<div class="circle" id="pcircles-54" data-text="Объём целевого финансирования<br> на 2016г." data-mesure="млн руб <br>" data-value="923" data-postfix=""></div>
										</div>
									</li>
								</ul>
							</div>
						</div>						
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Задачи фонда</h6>
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<ul class="pena_list">
							<li>способствовать развитию образования, науки,  культуры, искусства, просвещения, духовного развития личности;</li>
							<li>содействовать социальной поддержке и защите граждан, социальной реабилитации инвалидов и ветеранов; </li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<ul class="pena_list">
							<li>оказывать содействие профилактике и охране здоровья граждан, а также пропаганде здорового образа жизни;</li>
							<li>участвовать в реализации экологических программ и инициатив в сфере защиты окружающей среды.</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Благотворительная программа на 2016 год</h6>
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<ul class="pena_list row_pena_list">
							<li>9 направлений благотворительной деятельности</li>
							<li>923 млн руб. объем целевого финансирования</li>
							<li>37 целевых благотворительных программ в 11 регионах РФ</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Направления благотворительной деятельности</h6>
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>Культура</li>
							<li>Образование</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>Храмы и духовные ценности</li>
							<li>Спорт</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>Музей</li>
							<li>Здравоохранение</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>Музей</li>
							<li>Здравоохранение</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>География благотворительной деятельности</h6>
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>г. Москва</li>
							<li>Московская область</li>
							<li>г. Санкт Петербург</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>Краснодарский край</li>
							<li>Оренбургская область</li>
							<li>Ярославская область</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>г. Саратов</li>
							<li>г. Самара</li>
							<li>Республика Чечня</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<ul class="pena_list pena_list_four">
							<li>Республика Удмуртия</li>
							<li>Республика Карелия</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Официальный веб-сайт фонда</h6>
						<a href="www.safmar.ru">www.safmar.ru</a>
					</div>
				</div>
			</div><!--container-->
		</div>

	</div><!--wrapper-->        
        
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>