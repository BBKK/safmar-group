<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корпоративное управление");
?>

	<div id="header" data-options='{ direction: "normal"}'>
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg3_1_11.jpg');height: 100%"></div>
		<div class="blc_desc_foto">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Максим Панков</strong>, Золотая волна. Вечер на хребте Аибга. Сочинский национальный парк</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div>container		
		</div>		
	</div>
	<!--<div id="header" class="bg_main"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i btn_iW"></a>
				</div>
			</div>
		</div>
	</div><!--container-->

	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper">

		<div class="cnt_in cnt_persone">
			<!-- <div class="tabs tab_mng">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="tabs_corp">
								<ul class="part_cp">
									<li class="active"><h5>Совет директоров</h5></li>
									<li><h5>Менеджмент</h5></li>
								</ul>
							</div>
						</div>
					</div>
				</div><!--container
				<div class="row mp">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm">
						<div class="tabs__content tc1 active">
							<ul class="drd_list board_directors hd">
								<li>
									<div class="desc">
										<img src="/html/images/p1.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p2.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p3.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p4.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p5.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>	
								<li>
									<div class="desc">
										<img src="/html/images/p6.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p7.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p8.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p9.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p10.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p11.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>												
							</ul>
							
							<div class="slr_our_clients vs">
								<div class="swiper_foto swiper-container">
									<div class="swiper-wrapper">
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p1.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p2.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p3.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p4.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p5.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p6.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p7.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p8.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p9.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p10.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>
</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p11.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>
</div>
									</div>
								</div>
							</div><!--slr_our_clients				
						</div>
						<div class="tabs__content tc2">
							<ul class="drd_list board_directors hd">
								<li>
									<div class="desc">
										<img src="/html/images/p11.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p10.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p9.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p8.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p7.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p6.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p5.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p4.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p3.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p2.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="desc">
										<img src="/html/images/p1.jpg" alt="">
										<div class="desc_bg">
											<div class="desc_txt">
												<a href="card_ng.php">
													<h5>Андриянкин Олег Владимирович</h5>
													<p>Финансовый директор</p>
													<span class="trn"></span>
												</a>
											</div>
										</div>
									</div>
								</li>								
							</ul>
							
							<div class="slr_our_clients vs">
								<div class="swiper_foto2 swiper-container">
									<div class="swiper-wrapper">
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p1.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="#">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p2.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p3.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p4.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="#">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p5.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p6.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p7.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p8.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>													
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p9.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p10.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
										<div class="swiper-slide">
											<ul class="drd_list board_directors">
												<li>
													<div class="desc">
														<img src="/html/images/p11.jpg" alt="">
														<div class="desc_bg">
															<div class="desc_txt">
																<a href="card_ng.php">
																	<h5>Андриянкин Олег Владимирович</h5>
																	<p>Финансовый директор</p>
																	<span class="trn"></span>
																</a>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--slr_our_clients
						</div>
					</div>
				</div>
			</div> -->

			<?$APPLICATION->IncludeComponent("bitrix:news.list", "korporativnoe_upravlenie", Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
				"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"DISPLAY_DATE" => "N",	// Выводить дату элемента
				"DISPLAY_NAME" => "Y",	// Выводить название элемента
				"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
				"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"FIELD_CODE" => array(	// Поля
					0 => "",
					1 => "",
				),
				"FILTER_NAME" => "",	// Фильтр
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
				"IBLOCK_ID" => "2",	// Код информационного блока
				"IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"NEWS_COUNT" => "999",	// Количество новостей на странице
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Новости",	// Название категорий
				"PARENT_SECTION" => "",	// ID раздела
				"PARENT_SECTION_CODE" => "",	// Код раздела
				"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
				"PROPERTY_CODE" => array(	// Свойства
					0 => "DOLJNOST",
					1 => "",
				),
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
				"SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
				"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
				"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			), false );?>

			<div class="objectives_financial_group">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h6>Задачи финансовой группы</h6>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<ul class="pena_list">
								<li>Организация управления активами группы</li>
								<li class="vs">Участие в советах директоров и ключевых комитетах при советах<br>директоров компаний группы</li>
								<li class="hd">Участие в советах директоров и ключевых комитетах при советах директоров компаний группы</li>
								<li>Разработка KPIs и систем мотивации</li>
								<li>Организация рабочих групп и согласование ключевых инициатив Стратегический анализ</li>
								<li>Разработка и обновление стратегии группы</li>
								<li>Координация и согласование стратегий компаний</li>
								<li>Контроль соответствия операций на уровне бизнеса общей стратегии</li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<ul class="pena_list">
								<li>Финансовый контроль и риск-менеджмент</li>
								<li>Финансовый мониторинг, консолидация и управление капиталом</li>
								<li>Консолидация функций внутреннего контроля и риск-менеджмента</li>
								<li>Координация исполнения решений акционеров на уровне бизнеса</li>
								<li>Сопровождение проектов</li>
								<li>Разработка и реализация инвестиционных идей</li>
								<li>Координация и реализация инвестиционной активности группы на рынке</li>
								<li>Структурирование внешних и внутренних сделок</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div><!--wrapper-->        
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>