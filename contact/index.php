<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

		<div id="header" data-options='{ direction: "normal"}'>
			<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg_contacts2.jpg');height: 100%"></div>
			<div class="blc_desc_foto blc_desc_fotoW">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p><strong>Рафаэль Гатиятуллин</strong>, Река Актру осенью, Горный Алтай</p>
							<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
						</div>
					</div>
				</div><!--container-->		
			</div>			
		</div>
	<!--<div id="header" class="bg_contacts"></div>--><!--header-->
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="btnI">
						<a href="#" class="btn_i "></a>
					</div>
				</div>
			</div>								
		</div><!--container-->		
		
		<div class="blc_bg_menu">
			<div class="bg_menu">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="logo"><a href="/"><img src="/html/images/mlogo.png" alt=""></a></div>
							<div class="blc_lang">
								<ul class="lang">
									<li class="active"><a href="#">ru</a></li>
									<li>/</li>
									<li><a href="#">en</a></li>
								</ul>
							</div>
							<div class="navbar navbar-default navbar-static-top">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
                                                                <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                                "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                        ), false );?> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="wrapper">
			<div class="pensioner_contact">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2>Пенсионный</h2>
						</div>
					</div>
					<div class="row">	
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ul class="cnt">
								<li>
									<div class="desc">
										<a href="mailto:+74959098969">+7 495 909 89 69</a>
										<p>с 9.00 до 18.00 по Московскому времени</p>
									</div>
								</li>
								<li>
									<div class="desc">
										<p class="green">Адрес</p>
										<p class="vs">109240, Россия, г. Москва, ул. Верхняя<br>Радищевская, дом 16, строение 2</p>
										<p class="hd">109240, Россия, г. Москва, ул. Верхняя Радищевская, дом 16, строение 2</p>
									</div>								
								</li>
								<li>
									<div class="desc">
										<p class="green">Написать нам</p>
										<p><a href="mailto:cs@safmarpensions.ru">cs@safmarpensions.ru</a></p>
									</div>
								</li>
							</ul>
						</div>												
					</div>
				
				</div><!--container-->
			</div>		
		
			<div class="product_details_map">
				<div class="map-canvas" id="map10"></div>
				<span name='' text='Сафмар' url='#' x='37.6302766'  y='55.732798'></span>
			</div>
		</div><!--wrapper-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>