<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
preprintjs($arResult);

foreach($arResult["ITEMS"] as $key=>$item){
    $Items[$item["IBLOCK_SECTION_ID"]][] = $item;
}
?>

<div class="tabs tab_mng">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tabs_corp">
                    <ul class="part_cp">
                        <?foreach ($arResult["SECTION"] as $key => $section) {
                            if($key == 0){?>
                                <li class="active"><h5><?=$section["NAME"]?></h5></li>
                            <?}else{?>
                                <li><h5><?=$section["NAME"]?></h5></li>
                            <?}?>
                        <?}?>
                    </ul>
                </div>
            </div>
        </div>
    </div><!--container-->

    <div class="row mp">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm">
            <?foreach ($arResult["SECTION"] as $key => $section) {?>
                <?if($key == 0){
                    $class = "tabs__content tc".($key+1)." active";
                }else{
                    $class = "tabs__content tc".($key+1)."";
                }?>
                <div class="<?=$class?>">
                    <ul class="drd_list board_directors hd">
                        <?foreach ($Items[$section["ID"]] as $elem) {?>
                            <li>
                                <div class="desc">
                                    <img src="<?=$elem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                    <div class="desc_bg">
                                        <div class="desc_txt">
                                            <a href="<?=$elem["DETAIL_PAGE_URL"]?>">
                                                <h5><?=$elem["NAME"]?></h5>
                                                <?if(!empty($elem["PROPERTIES"]["DOLJNOST"]["VALUE"])){?><p><?=$elem["PROPERTIES"]["DOLJNOST"]["VALUE"]?></p><?}else{?><p class="pointers">.</p><?}?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?}?>
                    </ul>

                    <div class="slr_our_clients vs">
                        <div class="swiper_foto swiper-container">
                            <div class="swiper-wrapper">
                                <?foreach ($Items[$section["ID"]] as $elem) {?>
                                    <div class="swiper-slide">
                                        <ul class="drd_list board_directors">
                                            <li>
                                                <div class="desc">
                                                    <img src="<?=$elem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                                    <div class="desc_bg">
                                                        <div class="desc_txt">
                                                            <a href="<?=$elem["DETAIL_PAGE_URL"]?>">
                                                                <h5><?=$elem["NAME"]?></h5>
                                                                <?if(!empty($elem["PROPERTIES"]["DOLJNOST"]["VALUE"])){?><p><?=$elem["PROPERTIES"]["DOLJNOST"]["VALUE"]?></p><?}else{?><p class="pointers">.</p><?}?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                <?}?>

                            </div>
                        </div>
                    </div><!--slr_our_clients-->

                </div>
            <?}?>
        </div>
    </div>
</div>
