<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arOrder = array( "SORT" => "DESC", "ACTIVE_FROM" => "DESC"	);
$arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"]);
$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "SORT", "ACTIVE_FROM");
$res = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);
while($arFieldsSection = $res->GetNext()){
    $arResult["SECTION"][] = $arFieldsSection;
}

