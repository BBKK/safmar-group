<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="blc_schedule">

        <?foreach($arResult["ITEMS"] as $key => $arItem){?>
            <div class="blc_all_sch_txt sch_txt<?=$key+1?>">
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            </div>
        <?}?>

        <?foreach($arResult["ITEMS"] as $key => $arItem){?>
            <div class="blc_all_sch sch<?=$key+1?>">
                <p><?=$arItem["NAME"]?></p>
                <div class="vrt_line"></div>
                <div class="big_in_scircle"></div>
            </div>
        <?}?>

        <div class="big_all_crl">
            <div class="big_in_scircle big_allcrl1"></div>
            <div class="big_in_scircle big_allcrl2"></div>
            <div class="big_in_scircle big_allcrl3"></div>
            <div class="big_in_scircle big_allcrl4"></div>
            <div class="big_in_scircle big_allcrl5"></div>
            <div class="big_in_scircle big_allcrl6"></div>
            <div class="big_in_scircle big_allcrl7"></div>
            <div class="big_in_scircle big_allcrl8"></div>
            <div class="big_in_scircle big_allcrl9"></div>
            <div class="big_in_scircle big_allcrl10"></div>
            <div class="big_in_scircle big_allcrl11"></div>
            <div class="big_in_scircle big_allcrl12"></div>
            <div class="big_in_scircle big_allcrl13"></div>
            <div class="big_in_scircle big_allcrl14"></div>
        </div>

        <div class="blc_oundation">
            <p class="year ye1">
                <span>2011</span>
            </p>
            <p class="year ye2">
                <span>2012</span>
            </p>
            <p class="year ye3">
                <span>2013</span>
            </p>
            <p class="year ye4">
                <span>2014</span>
            </p>
            <p class="year ye5">
                <span>2015</span>
            </p>
            <p class="year ye6">
                <span>2016</span>
            </p>
        </div>
    </div>
</div>
