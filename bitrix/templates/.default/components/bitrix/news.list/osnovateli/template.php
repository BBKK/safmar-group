<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//preprint($arResult["ITEMS"]);
?>

<ul class="drd_list board_directors">
<?foreach($arResult["ITEMS"] as $arItem){?>
	<li>
		<div class="desc">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
			<div class="desc_bg">
				<div class="desc_txt">
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<h5><?=$arItem["NAME"]?></h5>
                                                <?if(!empty($arItem["PROPERTIES"]["DOLJNOST"]["VALUE"])){?><p><?=$arItem["PROPERTIES"]["DOLJNOST"]["VALUE"]?></p><?}else{?><p class="pointers">.</p><?}?>
						<span class="trn"></span>
					</a>
				</div>
			</div>
		</div>
	</li>
<?}?>
</ul>
