<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="tabs tab_corporate">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="blc_main_page_ttl">
					<p class="vs"><strong>Финансовая группа САФМАР</strong> - одна из крупнейших финансовых групп в России, включающая<br>банковские активы, страхование, лизинг, негосударственные пенсионные фонды, а также<br>нефинансовые активы.</p>
					<p class="hd"><strong>Финансовая группа САФМАР</strong> - одна из крупнейших финансовых групп в России, включающая банковские активы, страхование, лизинг, негосударственные пенсионные фонды, а также нефинансовые активы.</p>								
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="blc_nt_fx_cnt">
					<div class="blc_fix_fltr">
						<div class="blc_start_animate">
							<a href="javascript:void(0)" class="play_animate"></a>
						</div>
						<div class="tabs_corp">
							<ul class="part_cp ">
								<?foreach($arResult["ITEMS"] as $key => $arItem){?>
									<?if($key == 0){?>
										<li class="active" data-index="<?=$key+1?>">
											<h5><?=$arItem["NAME"]?></h5>
										</li>
									<?}else{?>
										<li data-index="<?=$key+1?>">
											<h5><?=$arItem["NAME"]?></h5>
										</li>
									<?}?>
								<?}?>
							</ul>
						</div>
					</div>
				</div>							
			</div>
		</div>
	</div><!--container-->
	<div class="row mp">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm" id="slideCircles">
			<div class="blc_all_main_tab">
				<?foreach($arResult["ITEMS"] as $key => $arItem){?>
					<?=$arItem["PREVIEW_TEXT"]?>
				<?}?>
			</div>
		</div>
	</div>
</div>
