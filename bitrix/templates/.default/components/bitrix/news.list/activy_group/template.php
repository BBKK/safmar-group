<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="tabs tab_corporate">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="blc_active_group_ttl">
					<h2>Активы группы</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="tabs_corp">
					<ul class="part_cp">
						<?foreach($arResult["ITEMS"] as $key => $arItem){?>
							<?if($key == 0){?>
								<li class="active" data-index="<?=$key+1?>">
									<h5><?=$arItem["NAME"]?></h5>
								</li>
							<?}else{?>
								<li data-index="<?=$key+1?>">
									<h5><?=$arItem["NAME"]?></h5>
								</li>
							<?}?>
						<?}?>
					</ul>
				</div>
			</div>
		</div>
	</div><!--container-->
	<div class="row mp">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm">
			<?foreach($arResult["ITEMS"] as $key => $arItem){?>
				<?=$arItem["PREVIEW_TEXT"]?>
			<?}?>
		</div>
	</div>
</div>
