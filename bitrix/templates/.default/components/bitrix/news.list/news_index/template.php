<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

global $month_name, $month_content, $news_content, $all_content, $paginator_content;
$year_now = '';
$year_now = date('Y');
/*$month_now = date('m');

if($month_now<4){
        $kv_now = 1;
}elseif($month_now<7){
        $kv_now = 2;
}elseif($month_now<9){
        $kv_now = 3;
}else{
        $kv_now = 4;
}*/

if(empty($_POST['count'])){
        $count_element = 12;
}else{
        $count_element = $_POST['count'];
}

$count = count($arResult["ITEMS"]) - 1;
foreach($arResult["ITEMS"] as $k=>$arItem):
        $date = explode('.',$arItem['DATE_ACTIVE_FROM']);
        $day = round($date[0]);
        $month = round($date[1]);
        $year = round($date[2]);

        if($month<4){
                $kv = 1;
        }elseif($month<7){
                $kv = 2;
        }elseif($month<9){
                $kv = 3;
        }else{
                $kv = 4;
        }
        if($day>9){
                $arItem['DAY'] = $day;
        }else{
                $arItem['DAY'] = '0'.$day;
        }
        $arItem['MONTH'] = $month;
        /*if($month>9){
                $arItem['MONTH'] = $month;
        }else{
                $arItem['MONTH'] = '0'.$month;
        }   */
        $arItem['YEAR'] = $year;
        if(!in_array($year,$years))
                $years[$year] = $year;
        $elements[$year][]=$arItem;
        $elements_all[] = $arItem;
endforeach;
arsort($years);
ksort($elements);
ksort($elements_all);

?>

                    <div class="row line_years">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="blc_news_tr blc_pln">
                                <div id="slider_year"></div>
                                <input type="text" id="inp_year" name='year' min-year='<?=min($years)?>' max-year='<?=max($years)?>'>
                            </div>
                            <div class="blc_news_tr blc_years_scl">
                                <ul class="era_list_years">
                                    <li class="sele_q active">
                                        <a href="javascript:return false;" class="zero_q quartal" data-attr="0">весь год</a>
                                    </li>
                                    <li class="sele_q">
                                        <a href="javascript:return false;" class="first_q quartal" data-attr="1">янв.-мар.</a>
                                    </li>
                                    <li class="sele_q">
                                        <a href="javascript:return false;" class="second_q quartal" data-attr="2">апр.-июн.</a>
                                    </li>
                                    <li class="sele_q">
                                        <a href="javascript:return false;" class="third_q quartal" data-attr="3">июл.-сен.</a>
                                    </li>
                                    <li class="sele_q">
                                        <a href="javascript:return false;" class="fourth_q quartal" data-attr="4">окт.-дек.</a>
                                    </li>
                                </ul>
                                <div class="arw_ln arw_list_news_prev">
                                    <a href="#" class="prev prev_q"></a>
                                </div>
                                <div class="arw_ln arw_list_news_next">
                                    <a href="#" class="next next_q"></a>
                                </div>
                            </div>
                            <div class="blc_news_tr blc_rss">
                                <a href="#" class="rss">Подписаться на новости</a>
                            </div>
                        </div>
                    </div>
                    <div class="blc_link_on_news">
                        <?foreach($elements[$year_now] as $k=>$item){?>
                            <?if($k%2 == 0){?><div class="row line_two_news"><?}?>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <span class="date"><?=$item['DAY'].' '.$month_name[$item['MONTH']][1].' '.$item['YEAR']?></span>
                                        <p><?=$item['NAME']?></p>
                                        <a href="<?=$item['DETAIL_PAGE_URL']?>">Подробнее</a>
                                    </div>
                                <?if($k%2 == 1){?></div><?}?>                                    
                                
                        <?}?>
                    </div><!--blc_link_on_news-->
