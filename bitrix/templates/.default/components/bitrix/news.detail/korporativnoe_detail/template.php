<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="cnt_in cnt_persone">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="go_back">
					<a href="/korporativnoe-upravlenie/" class="back_section">
						Назад к разделу
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="persone_img">
					<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
				</div>
				<h6><?=$arResult["NAME"]?></h6>
				<?=$arResult["DETAIL_TEXT"]?>
			</div>
		</div>
	</div><!--container-->
</div>