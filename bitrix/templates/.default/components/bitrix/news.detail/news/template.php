<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

global $month_name;

$date = explode('.',$arResult['ACTIVE_FROM']);
$day = round($date[0]);
$month = round($date[1]);
if($day>9){
        $arResult['DAY'] = $day;
}else{
        $arResult['DAY'] = '0'.$day;
}

$arResult['MONTH'] = $month;
$arResult['YEAR'] = round($date[2]);


$arSelect = Array("ID", "NAME", 'CODE', "DATE_ACTIVE_FROM", 'DETAIL_PAGE_URL');
$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "<=DATE_ACTIVE_FROM"=>$arResult['ACTIVE_FROM'], '!=ID'=>$arResult['ID'], "ACTIVE"=>"Y");
$rsElement = CIBlockElement::GetList(Array('DATE_ACTIVE_FROM'=>'DESC', 'DATE_CREATE'=>'DESC'), $arFilter, false, Array("nPageSize"=>1), $arSelect);
if($arElement = $rsElement->GetNext()){
        $pref = $arElement;
}

$arSelect = Array("ID", "NAME", 'CODE', "DATE_ACTIVE_FROM", 'DETAIL_PAGE_URL');
$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], ">=DATE_ACTIVE_FROM"=>$arResult['ACTIVE_FROM'], '!=ID'=>$arResult['ID'], "ACTIVE"=>"Y");
$rsElement = CIBlockElement::GetList(Array('DATE_ACTIVE_FROM'=>'ASC', 'DATE_CREATE'=>'ASC'), $arFilter, false, Array("nPageSize"=>1), $arSelect);
if($arElement = $rsElement->GetNext()){
        $next = $arElement;
}

?>


        <div class="row news_block_item">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="dd_list">
                                <li>
                                    <span class="date sigle_new_descr_date"><?=$arResult['DAY'].' '.$month_name[$arResult['MONTH']][1].' '.$arResult['YEAR']?></span>
                                    <h5 class="sigle_new_descr_txt"><?=$arResult['NAME']?></h5>
                                </li>
                                <li>
                                    <div class="blc_all_desc_news">
                                        <p class="blc_all_desc_news_txt"><?=$arResult['DETAIL_TEXT']?></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
        </div>
        <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="<?=$pref['DETAIL_PAGE_URL']?>" class="sleft"></a>
                        <?if(!empty($next)){?><a href="<?=$next['DETAIL_PAGE_URL']?>" class="sright"></a><?}?>                                
                            <ul class="np_list">
                                <li>
                                        <?if(!empty($pref)){?>
                                                <a href="<?=$pref['CODE']?>" class="prev prev_q_singl">Предыдущая</a>
                                        <?}else{?>
                                                <a href="#" class="prev prev_q_singl hide">Предыдущая</a>
                                        <?}?>
                                </li>
                                <li>
                                        <?if(!empty($next)){?>
                                               <a href="<?=$next['CODE']?>" class="next next_q_singl">Следующая</a>
                                        <?}else{?>
                                               <a href="#" class="next next_q_singl hide">Следующая</a>
                                        <?}?>
                                </li>
                                <li>
                                    <a href="#" class="soc_shara">Поделиться</a>
                                </li>
                            </ul>
                </div>
        </div>

