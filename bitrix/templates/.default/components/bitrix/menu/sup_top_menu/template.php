<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
        <div class="navIn">
                <ul class="nav_in" class="dropdown-menu">
                        <?foreach($arResult as $arItem){?>
                                <li<?if($arItem['SELECTED'] == 1){?> class="active"<?}?>>
                                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                                </li>                        
                        <?}?>
                </ul>
        </div>
<?}?>
	