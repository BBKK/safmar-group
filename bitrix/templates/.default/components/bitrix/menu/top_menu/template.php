<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
        <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                        <?foreach($arResult as $num=>$arItem){?>
                                <li<?if($arItem['SELECTED'] == 1){?> class="active"<?}?>>
                                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>   
                                        <?if($num<2){?>
                                                <?$APPLICATION->IncludeComponent("bitrix:menu", "sup_top_menu", Array(
                                                                "ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
                                                                "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                        ), false );?>                                           
                                        <?}?>
                                </li>                                
                        <?}?>
                </ul>
                <span class="line_menu"></span>
        </div><!--/.nav-collapse -->
<?endif?>