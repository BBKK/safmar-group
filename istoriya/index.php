<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("история");
?>
        <script src="/html/js/istoriya.js"></script>
	<div id="header" data-options='{ direction: "normal"}' class="">
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg3_1_11.jpg');height: 100%"></div>
		<div class="blc_desc_foto">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Максим Панков</strong>, Золотая волна. Вечер на хребте Аибга. Сочинский национальный парк</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div><!--container-->		
		</div>		
	</div>

	<!--<div id="header" class="bg_main"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i btn_iW"></a>
				</div>
			</div>
		</div>
	</div><!--container-->


	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?> 
						</div><!--/.nav-collapse -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper">

		<div class="history_page">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h2>История группы</h2>
					</div>
				</div>

				<div class="row">
					<?$APPLICATION->IncludeComponent("bitrix:news.list", "istoriya", Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
						"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
						"DISPLAY_DATE" => "N",	// Выводить дату элемента
						"DISPLAY_NAME" => "Y",	// Выводить название элемента
						"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"FIELD_CODE" => "",	// Поля
						"FILTER_NAME" => "",	// Фильтр
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
						"IBLOCK_ID" => "3",	// Код информационного блока
						"IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
						"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"NEWS_COUNT" => "14",	// Количество новостей на странице
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Новости",	// Название категорий
						"PARENT_SECTION" => "",	// ID раздела
						"PARENT_SECTION_CODE" => "",	// Код раздела
						"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
						"PROPERTY_CODE" => "",	// Свойства
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
						"SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
						"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
						"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
					), false );?>
					<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="blc_schedule">
						
							
							<div class="blc_all_sch_txt sch_txt1">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».</p>
							</div> 
							

							<div class="blc_all_sch_txt sch_txt2">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».2</p>
							</div> 
							
							
							<div class="blc_all_sch_txt sch_txt3">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».3</p>
							</div> 
							

							<div class="blc_all_sch_txt sch_txt4">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».4</p>
							</div> 	
						
							
							<div class="blc_all_sch_txt sch_txt5">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».5</p>
							</div> 
							

							<div class="blc_all_sch_txt sch_txt6">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».6</p>
							</div> 

						
							<div class="blc_all_sch_txt sch_txt7">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».7</p>
							</div>
						
							
							<div class="blc_all_sch_txt sch_txt8">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».8</p>
							</div> 
							

							<div class="blc_all_sch_txt sch_txt9">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».9</p>
							</div> 
							
							
							<div class="blc_all_sch_txt sch_txt10">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».10</p>
							</div> 
							

							<div class="blc_all_sch_txt sch_txt11">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».11</p>
							</div> 	
						
							
							<div class="blc_all_sch_txt sch_txt12">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».12</p>
							</div> 
							

							<div class="blc_all_sch_txt sch_txt13">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».13</p>
							</div> 

						
							<div class="blc_all_sch_txt sch_txt14">
								<p>Создание и внедрение плана финансового оздоровления для банков в рамках финансовой реабилитации Затраты были сокращены по меньшей мере на 30%; бизнес был передан БИНБАНКу; региональные банки (Аккобанк, Тверьуниверсалбанк, СКА-Банк) были успешно интегрированы в технологическую платформу БИНБАНКа в качестве дочерних банков; был инициирован проект по закрытию филиальной сети АО «РОСТ БАНК».14</p>
							</div> 								
							
							
							
							
						
							<div class="blc_all_sch sch1">
								<p>НПФ Доверие</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch2">
								<p>НПФ Образование и наука</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch3">
								<p>Башинвестбанк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch4">
								<p>НПФ РегионФонд</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>	
							
							<div class="blc_all_sch sch5">
								<p>DNB банк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch6">
								<p>МоскомПриватбанк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch7">
								<p>Группа РОСТ банк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch8">
								<p>НПФ Райффайзен</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>	
							
							<div class="blc_all_sch sch9">
								<p>Компания Европлан</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>							
							
							<div class="blc_all_sch sch10">
								<p>Пробизнесбанк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch11">
								<p>УралПриватБанк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>

							<div class="blc_all_sch sch12">
								<p>НПФ САФМАР</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>
							
							<div class="blc_all_sch sch13">
								<p>НПФ Европейский</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>

							<div class="blc_all_sch sch14">
								<p>МДМ банк</p>
								<div class="vrt_line"></div>
								<div class="big_s_circle"></div>
								<div class="big_in_scircle"></div>
							</div>							
						
							<div class="blc_oundation">
								<p class="year ye1">
									<span>2011</span>
								</p>	
								<p class="year ye2">
									<span>2012</span>
								</p>
								<p class="year ye3">
									<span>2013</span>
								</p>
								<p class="year ye4">
									<span>2014</span>
								</p>
								<p class="year ye5">
									<span>2015</span>
								</p>
								<p class="year ye6">
									<span>2016</span>
								</p>															
							</div>
						</div>
					</div> -->
				</div>
			</div><!--container-->
		</div>

	</div><!--wrapper-->        
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>