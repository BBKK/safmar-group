<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Раскрытие информации");
?>
	<div id="header" data-options='{ direction: "normal"}' class="">
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg_main.jpg');height: 100%"></div>
		<div class="blc_desc_foto">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Сергей Королев</strong>, Полночь в Хибинах</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div><!--container-->		
		</div>		
	</div>

	<!--<div id="header" class="bg_main"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i btn_iW"></a>
				</div>
			</div>
		</div>
	</div><!--container-->


	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?> 							
						</div><!--/.nav-collapse -->
					</div>
				</div>
			</div>
		</div>
	</div>

        <div id="wrapper">
 		<div class="blc_lim_lib infi_disc">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h2>Акционерное общество<br>«Финансовая группа САФМАР»</h2>
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Полное наименование на русском языке: </h6>
						<p>Акционерное общество «Финансовая группа САФМАР» </p>
						<h6>Сокращенное наименование на русском языке:</h6>
						<p>АО «ФГ САФМАР»</p>
						<h6>Полное наименование на иностранном (английском) языке:</h6>
						<p>Joint Stock Company "SAFMAR Financial Group"</p>	
						<h6>Сокращенное наименование на иностранном (английском) языке:</h6>
						<p>SAFMAR FG</p>					
					</div>
				</div>
				<div class="row pdng">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Дата государственной регистрации:</h6>
						<p>06 июня 2007 года</p>
						<h6>Свидетельство о государственной регистрации:</h6>
						<p>серия 77 № 009435094, выдано Межрайонной инспекцией Федеральной налоговой службы № 46 по г.Москве 06 июня 2007 года</p>																<h6>Адрес: </h6>
						<p>115172, г. Москва, набережная Котельническая, д. 33, стр.1</p>	
						<h6>ИНН:</h6>
						<p>7713622487</p>					
					</div>
				</div>	
				<div class="row pdng">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h6>Дата лицензии на осуществление деятельности по управлению ценными бумагами: </h6>
						<p>24 марта  2009 года</p>
						<h6>Номер лицензии на осуществление деятельности по управлению ценными бумагами: </h6>
						<p><a href="/html/doc/lic.pdf">045-12094-001000</a></p>
						<h6>Орган, выдавший лицензию:</h6>
						<p>ФСФР России</p>	
						<h6>Срок действия лицензии:</h6>
						<p>без ограничения срока действия</p>					
					</div>
				</div>
				
				<div class="row pdng">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="blc_national">
						<h6>Член СРО Некоммерческое партнерство "Национальная лига управляющих"</h6>
							<ul>
								<li>ФИО Генерального директора: Миракян Авет Владимирович</li>
								<li>Контактные телефоны: <a href="tel:+74959098969">+7(495)909-89-69</a></li>
								<li>Адрес электронной почты: <a href="mailto:financproject@mail.ru">financproject@mail.ru</a></li>
							</ul>
						</div>				
					</div>
				</div>					

				<div class="row pdng">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="blc_required_information">
							<h6>Обязательная информация</h6>	
							<ul>
								<li>Бухгалтерская отчетность
									<ul>
										<li>
											<p><a href="/html/doc/audit_2015.pdf">Аудиторское заключение о бухгалтерской (финансовой) отчетности АО ФГ САФМАР за 2015 год</a></p>
											<span> (Дата раскрытия 15.04.2016 г. Доступно до 15.04.2019 г.)</span>
										</li>
										<li>
											<p><a href="/html/doc/bb_2015.pdf">Бухгалтерский баланс АО ФГ САФМАР на 31 декабря 2015 г., квитанция о приеме бухгалтерской отчетности за 2015 г.</a></p>
											<span>(Дата раскрытия 15.04.2016 г. Доступно до 15.04.2019 г.)</span>										
										</li>
										<li>
											<p><a href="/html/doc/fin_result_2015.pdf">Отчет о финансовых результатах АО ФГ САФМАР за период с 01 января по 31 декабря 2015 г.</a></p>
											<span>(Дата раскрытия 15.04.2016 г. Доступно до 15.04.2019 г.)</span>										
										</li>
									</ul>
								</li>
								<li>Расчет размера собственных средств
									<ul>
										<li>
											<p><a href="/html/doc/rss20160229.pdf">Расчет размера собственных средств на 29-02-2016</a></p>
											<span>(Информация размещена 31-03-2016 в 16:15 и будет доступна до 31-03-2019)</span>
										</li>
										<li>
											<p><a href="/html/doc/rss20160131.pdf">Расчет размера собственных средств на 31-01-2016</a></p>
											<span>(Информация размещена 29-02-2016 в 16:35 и будет доступна до 28-02-2019)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20151231.pdf">Расчет размера собственных средств на 31-12-2015</a></p>
											<span>(Информация размещена 28-01-2016 в 17:50 и будет доступна до 31-01-2017)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20151130.pdf">Расчет размера собственных средств на 30-11-2015</a></p>
											<span> (Информация размещена 18-12-2015 в 11:00 и будет доступна до 31-12-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20151031.pdf">Расчет размера собственных средств на 31-10-2015</a></p>
											<span>(Информация размещена 19-11-2015 в 17:40 и будет доступна до 30-11-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20150930.pdf">Расчет размера собственных средств на 30-09-2015</a></p>
											<span>(Информация размещена 22-10-2015 в 17:40 и будет доступна до 31-10-2016)</span>
										</li>
										<li>
											<p><a href="/html/doc/rss20150831.pdf">Расчет размера собственных средств на 31-08-2015</a></p>
											<span>(Информация размещена 25-09-2015 в 17:40 и будет доступна до 30-09-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20150731.pdf">Расчет размера собственных средств на 31-07-2015</a></p>
											<span>(Информация размещена 24-08-2015 в 17:59 и будет доступна до 31-08-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20150630.pdf">Расчет размера собственных средств на 30-06-2015</a></p>
											<span> (Информация размещена 16-07-2015 в 15:00 и будет доступна до 31-07-2016)</span>
										</li>
										<li>
											<p><a href="/html/doc/rss20150531.pdf">Расчет размера собственных средств на 31-05-2015</a></p>
											<span> (Информация размещена 19-06-2015 в 16:40 и будет доступна до 30-06-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20150430.pdf">Расчет размера собственных средств на 30-04-2015</a></p>
											<span>(Информация размещена 29-05-2015 в 18:30 и будет доступна до 31-05-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20150331.pdf">Расчет размера собственных средств на 31-03-2015</a></p>
											<span>(Информация размещена 29-04-2015 в 12:30 и будет доступна до 30-04-2016)</span>
										</li>
										<li>
											<p><a href="/html/doc/rss20150228.pdf">Расчет размера собственных средств на 28-02-2015</a></p>
											<span>(Информация размещена 20-03-2015 в 12:30 и будет доступна до 31-03-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20150131.pdf">Расчет размера собственных средств на 31-01-2015</a></p>
											<span>(Информация размещена 27-02-2015 в 17:30 и будет доступна до 29-02-2016)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss201412312.pdf">Расчет размера собственных средств на 31-12-2014</a></p>
											<span> (Информация размещена 29-01-2015 в 10:30 и будет доступна до 31-01-2016)</span>
										</li>
										<li>
											<p><a href="/html/doc/rss20141130.pdf">Расчет размера собственных средств на 30-11-2014</a></p>
											<span> (Информация размещена 16-12-2014 в 11:10 и будет доступна до 31-12-2015)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20141031.pdf">Расчет размера собственных средств на 31-10-2014</a></p>
											<span>(Информация размещена 21-11-2014 в 17:40 и будет доступна до 30-11-2015)</span>									
										</li>
										<li>
											<p><a href="/html/doc/rss20140930.pdf">Расчет размера собственных средств на 30-09-2014</a></p>
											<span> (Информация размещена 29-10-2014 в 12:30 и будет доступна до 31-10-2015)</span>
										</li>
									</ul>								
								</li>
							</ul>
							<ul class="info_tw_list">
								<li>
									<p><a href="/html/doc/pii.PDF">Перечень инсайдерской информации</a></p>
								</li>
								<li>
									<p><a href="/html/doc/rplki2.pdf">Регламент признания лиц квалифицированными инвесторами</a></p>
								</li>
								<li>
									<p><a href="/html/doc/FATCA.pdf">Критерии выявления клиента-иностранного налогоплательщика в целях FATCA</a></p>
								</li>
								<li>
									<p><a href="/html/doc/kpoducb.pdf">Консолидированные правила осуществления деятельности по управлению ценными бумагами Акционерного общества "Финансовая группа САФМАР" </a>
							</p><span>(дата размещения Правил 21.03.2016 г., дата вступления Правил в силу 01.04.2016 г.)</span>
								</li>
							</ul>
						</div>
					</div>
				</div>								
			</div><!--container-->
		</div>               
        </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>