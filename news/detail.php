<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle();
?>
    <div id="header" data-options='{ direction: "normal"}' class="">
        <div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg_news.jpg'); height: 100%"></div>
        <div class="blc_desc_foto">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p><strong>Сергей Анисимов</strong>, Лед Байкала</p>
                        <p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
                    </div>
                </div>
            </div><!--container-->
        </div>
    </div>

    <!--<div id="header" class="bg_main"></div>--><!--header-->

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="btnI">
                    <a href="#" class="btn_i btn_iW"></a>
                </div>
            </div>
        </div>
    </div><!--container-->


    <div class="blc_bg_menu">
        <div class="bg_menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="logo">
                            <a href="/"><img src="/html/images/mlogo.png" alt=""></a>
                        </div>
                        <div class="blc_lang">
                            <ul class="lang">
                                <li class="active"><a href="#">ru</a></li>
                                <li>/</li>
                                <li><a href="#">en</a></li>
                            </ul>
                        </div>
                        <div class="navbar navbar-default navbar-static-top">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                                                "MAX_LEVEL" => "0",    // Уровень вложенности меню
                                        ), false );?>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="wrapper">

        <div class="history_page">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="go_back">
                            <a href="/news/" class="back_section">
                                Назад к разделу
                            </a>
                        </div>
                    </div>
                </div>
                <div class="blc_desc_detail">                    
                        <?$APPLICATION->IncludeComponent("bitrix:news.detail", "news", Array(
                                "IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
                                "IBLOCK_ID" => "6",	// Код информационного блока
                                "ELEMENT_ID" => "",	// ID новости
                                "ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],	// Код новости
                                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                "FIELD_CODE" => array(	// Поля
                                        0 => "",
                                        1 => "undefined",
                                        2 => "",
                                ),
                                "PROPERTY_CODE" => array(	// Свойства
                                        0 => "",
                                        1 => "undefined",
                                        2 => "",
                                ),
                                "IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                                "AJAX_MODE" => "N",	// Включить режим AJAX
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                                "SET_BROWSER_TITLE" => "Y",
                                "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                                "SET_META_KEYWORDS" => "Y",
                                "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                                "SET_META_DESCRIPTION" => "Y",
                                "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                                "ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
                                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                                "DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
                                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                                "USE_SHARE" => "N",	// Отображать панель соц. закладок
                                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                                "PAGER_TITLE" => "Страница",	// Название категорий
                                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                ),
                                false
                        );?>
                </div>
            </div><!--container-->
        </div>

    </div><!--wrapper-->
 <?$APPLICATION->IncludeComponent("bitrix:news.detail", "news_detail", Array(
                                "IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
                                "IBLOCK_ID" => "6",	// Код информационного блока
                                "ELEMENT_ID" => "",	// ID новости
                                "ELEMENT_CODE" => 'chistaya_pribyl_binbanka_po_itogam_2015_goda_vyrosla_na_139_prevysiv_4_5_mlrd_rubley_5',	// Код новости
                                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                "FIELD_CODE" => array(	// Поля
                                        0 => "",
                                        1 => "undefined",
                                        2 => "",
                                ),
                                "PROPERTY_CODE" => array(	// Свойства
                                        0 => "",
                                        1 => "undefined",
                                        2 => "",
                                ),
                                "IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                                "AJAX_MODE" => "N",	// Включить режим AJAX
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                                "SET_BROWSER_TITLE" => "Y",
                                "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                                "SET_META_KEYWORDS" => "Y",
                                "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                                "SET_META_DESCRIPTION" => "Y",
                                "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                                "ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
                                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                                "DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
                                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                                "USE_SHARE" => "N",	// Отображать панель соц. закладок
                                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                                "PAGER_TITLE" => "Страница",	// Название категорий
                                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                ),
                                false
                        );?>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>