<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пресс-центр");
?>

        <script src="/html/js/news.js"></script>
    <div id="header" data-options='{ direction: "normal"}' class="">
        <div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg_news.jpg'); height: 100%"></div>
        <div class="blc_desc_foto">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p><strong>Сергей Анисимов</strong>, Лед Байкала</p>
                        <p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
                    </div>
                </div>
            </div><!--container-->
        </div>
    </div>

    <!--<div id="header" class="bg_main"></div>--><!--header-->

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="btnI">
                    <a href="#" class="btn_i btn_iW"></a>
                </div>
            </div>
        </div>
    </div><!--container-->


    <div class="blc_bg_menu">
        <div class="bg_menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="logo">
                            <a href="/"><img src="/html/images/mlogo.png" alt=""></a>
                        </div>
                        <div class="blc_lang">
                            <ul class="lang">
                                <li class="active"><a href="#">ru</a></li>
                                <li>/</li>
                                <li><a href="#">en</a></li>
                            </ul>
                        </div>
                        <div class="navbar navbar-default navbar-static-top">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",    // Уровень вложенности меню
                                                                ), false );?>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="wrapper">

        <div class="history_page">
            <div class="container">

                <div class="blc_all_news">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h2>Новости</h2>
                        </div>
                    </div>
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list", 
                                "news_index", 
                                array(
                                        "DISPLAY_DATE" => "Y",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "Y",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "CONTENT",
                                        "IBLOCK_ID" => "6",
                                        "NEWS_COUNT" => "10000",
                                        "SORT_BY1" => "ACTIVE_FROM",
                                        "SORT_ORDER1" => "ASC",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "",
                                        "FIELD_CODE" => array(
                                                0 => "ID",
                                                1 => "CODE",
                                                2 => "NAME",
                                                3 => "SORT",
                                                4 => "PREVIEW_TEXT",
                                                5 => "PREVIEW_PICTURE",
                                                6 => "DETAIL_TEXT",
                                                7 => "DETAIL_PICTURE",
                                                8 => "DATE_ACTIVE_FROM",
                                                9 => "IBLOCK_TYPE_ID",
                                                10 => "IBLOCK_ID",
                                                11 => "DATE_CREATE",
                                                12 => "",
                                        ),
                                        "PROPERTY_CODE" => array(
                                                0 => "",
                                                1 => "",
                                        ),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "INCLUDE_SUBSECTIONS" => "Y",
                                        "CACHE_TYPE" => "N",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_NOTES" => "",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "Y",
                                        "PAGER_TEMPLATE" => "page",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "N",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "AJAX_OPTION_ADDITIONAL" => ""
                                ),
                                false
                        );?>                        
                    
                </div>
            </div><!--container-->
        </div>

    </div><!--wrapper-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
