<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сафмар");
?>

	<div id="header" data-options='{ direction: "normal"}'>
		<div class="divimage dzsparallaxer--target position-absolute " style="background-image: url('/html/images/bg_main.jpg');height: 100%"></div>
		<div class="blc_desc_foto blc_desc_fotoW">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p><strong>Сергей Королев</strong>, Полночь в Хибинах</p>
						<p>Фотоконкурс “Дикая природа России” 2015 года National Geographic</p>
					</div>
				</div>
			</div><!--container-->		
		</div>		
	</div>
	<!--<div id="header" class="bg_active"></div>--><!--header-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="btnI">
					<a href="#" class="btn_i"></a>
				</div>
			</div>
		</div>
	</div><!--container-->

	<div class="blc_bg_menu">
		<div class="bg_menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="logo">
							<a href="/"><img src="/html/images/mlogo.png" alt=""></a>
						</div>
						<div class="blc_lang">
							<ul class="lang">
								<li class="active"><a href="#">ru</a></li>
								<li>/</li>
								<li><a href="#">en</a></li>
							</ul>
						</div>
						<div class="navbar navbar-default navbar-static-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
                                                        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                                                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                                                        "MAX_LEVEL" => "0",	// Уровень вложенности меню
                                                                ), false );?>                                                        
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper">

		<div class="blc_main_page">
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "fin_group_main_page", Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
				"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"DISPLAY_DATE" => "N",	// Выводить дату элемента
				"DISPLAY_NAME" => "Y",	// Выводить название элемента
				"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
				"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"FIELD_CODE" => "",	// Поля
				"FILTER_NAME" => "",	// Фильтр
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
				"IBLOCK_ID" => "4",	// Код информационного блока
				"IBLOCK_TYPE" => "CONTENT",	// Тип информационного блока (используется только для проверки)
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"NEWS_COUNT" => "5",	// Количество новостей на странице
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Новости",	// Название категорий
				"PARENT_SECTION" => "",	// ID раздела
				"PARENT_SECTION_CODE" => "",	// Код раздела
				"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
				"PROPERTY_CODE" => "",	// Свойства
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
				"SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
				"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
				"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
			), false );?>
			<!-- <div class="tabs tab_corporate">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="blc_main_page_ttl">
								<p class="vs"><strong>Финансовая группа САФМАР</strong> - одна из крупнейших финансовых групп в России, включающая<br>банковские активы, страхование, лизинг, негосударственные пенсионные фонды, а также<br>нефинансовые активы.</p>
								<p class="hd"><strong>Финансовая группа САФМАР</strong> - одна из крупнейших финансовых групп в России, включающая банковские активы, страхование, лизинг, негосударственные пенсионные фонды, а также нефинансовые активы.</p>								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="blc_nt_fx_cnt">
								<div class="blc_fix_fltr">
									<div class="blc_start_animate">
										<a href="javascript:void(0)" class="play_animate"></a>
									</div>
									<div class="tabs_corp">
										<ul class="part_cp ">
											<li class="active" data-index="1">
												<h5>Банки</h5>
											</li>
											<li data-index="2">
												<h5>Пенсионные фонды</h5>
											</li>
											<li data-index="3">
												<h5>Лизинг</h5>
											</li>
											<li data-index="4">
												<h5>Страхование</h5>
											</li>
											<li data-index="5">
												<h5>Благотворительность</h5>
											</li>
										</ul>
									</div>
								</div>
							</div>							
						</div>
					</div>
				</div><!--container
				<div class="row mp">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pm" id="slideCircles">
						<div class="blc_all_main_tab">
							<div class="tabs__content tc1 active" data-index="1">
								<div class="pensioner_admin_circle_main">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="blc_main_desc_tab">
													<p>Одна из крупнейших финансовых групп в России, включающая банковские активы, страхование, лизинг, негосударственные пенсионные фонды, а также нефинансовые активы.</p>
													<p>Общий размер активов более 1 трлн руб. ТОП-3 среди частных российских банков по размеру активов</p>
												</div>
												<ul class="number_participants">
													<li>
														<div class="desc" id="circle11" >
															<div class="circle" id="pcircles-11" data-text="Точек продаж" data-mesure="шт" data-value="550" data-postfix=">"></div>
														</div>
													</li>
													<li>
														<div class="desc" id="circle21" >
															<div class="circle" id="pcircles-12" data-text="Количество сотрудников" data-mesure="чел" data-value="15000" data-postfix=">"></div>
														</div>									
													</li>
													<li>
														<div class="desc" id="circle31" >
															<div class=" circle" id="pcircles-13" data-text="Клиентов физических<br>лиц" data-mesure="чел" data-value="2000000" data-postfix="<br>>"></div>
															<div class="circle" id="pcircles-14" data-text="Клиентов юридических<br>лиц" data-mesure="чел" data-value="85000" data-postfix="<br>>"></div>
														</div>									
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tabs__content tc2" data-index="2">
								<div class="pensioner_admin_circle_main">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="blc_main_desc_tab">
													<p>Одна из крупнейших групп пенсионных фондов в России, осуществляющая обязательное пенсионное страхование и негосударственное пенсионное обеспечение, в том числе досрочное негосударственное пенсионное обеспечение..</p>
													<p>Опыт работы на пенсионном рынке более 22 лет</p>
												</div>
												<ul class="number_participants">
													<li>
														<div class="desc" id="circle12">
															<div class="circle" id="pcircles-21" data-text="Точек продаж" data-mesure="шт" data-value="10" data-postfix=">"></div>
														</div>
													</li>
													<li>
														<div class="desc" id="circle22">
															<div class="circle" id="pcircles-22" data-text="Количество сотрудников" data-mesure="чел" data-value="134" data-postfix=">"></div>
														</div>
													</li>
													<li>
														<div class="desc" id="circle32">
															<div class=" circle" id="pcircles-23" data-text="Клиентов физических<br>лиц" data-mesure="чел" data-value="2000000" data-postfix="<br>>"></div>
															<div class="circle" id="pcircles-24" data-text="Клиентов юридических<br>лиц" data-mesure="чел" data-value="88000" data-postfix="<br>>"></div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tabs__content tc3" data-index="3">
								<div class="pensioner_admin_circle_main">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="blc_main_desc_tab">
													<p>ПАО «Европлан» – крупнейшая негосударственная автолизинговая компания, предоставляющая лизинговые услуги для физических и юридических лиц.</p>
													<p>Рейтинг Fitch: долгосрочный РДЭ в иностранной и национальной валюте: «BB». 75 филиалов, представительств и офисов в 72 городах Российской Федерации.</p>
												</div>
												<ul class="number_participants">
													<li>
														<div class="desc" id="circle13">
															<div class="circle" id="pcircles-32" data-text="Действующих клиентов" data-mesure="шт" data-value="17000" data-postfix=""></div>
															<div class="circle" id="pcircles-31" data-text="Клиентов по<br>сделкам" data-mesure="шт" data-value="57000" data-postfix=""></div>
														</div>
													</li>
													<li>
														<div class="desc" id="circle23">
															<div class="circle" id="pcircles-33" data-text="Совершонных сделок" data-mesure="шт<br>" data-value="200000" data-postfix=">"></div>
														</div>
													</li>
													<li>
														<div class="desc" id="circle33">
															<div class="circle" id="pcircles-34" data-text="Инвистиции в<br>лизинг" data-mesure="млрд руб" data-value="26.9" data-postfix="<br>>"></div>
															<div class=" circle" id="pcircles-35" data-text="Активы" data-mesure="млрд руб" data-value="42.4" data-postfix=""></div>

														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>							
							</div>
							<div class="tabs__content tc4" data-index="4">
								<div class="pensioner_admin_circle_main">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="blc_main_desc_tab">
													<p>ООО «БИН Страхование» – универсальная страховая компания
		Работает на рынке с августа 2000 года.  Более 50 страховых продуктов для физических и юридических лиц различных отраслей.</p>
													<p>Представлена в 26 городах Российской Федерации</p>
												</div>
												<div class="blc_cut_circle">
													<div class="cr1"></div>
													<div class="cr2"></div>
													<div class="cr3"></div>
													<div class="cr4"></div>
													<div class="cr5"></div>
													<div class="cr6"></div>
													<div class="cr7"></div>
													<div class="cr8"></div>
													<div class="statistic st1">
														<p>финансовые риски</p>
														<span>11%</span>
													</div>
													<div class="statistic st2">
														<p>добровольное медицинское страхование</p>
														<span>12%</span>
													</div>
													<div class="statistic st3">
														<p>ОСАГО</p>
														<span>13%</span>
													</div>
													<div class="statistic st4">
														<p>имущество юридических лиц</p>
														<span>16%</span>
													</div>
													<div class="statistic st5">
														<p>несчастные случаи и болезни</p>
														<span>17%</span>
													</div>
													<div class="statistic st6">
														<p>ответственность</p>
														<span>17%</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tabs__content tc5" data-index="5">
							<div class="pensioner_admin_circle_main">
								<div class="container">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="blc_main_desc_tab">
												<p>Одна из крупнейших финансовых групп в России, включающая банковские активы, страхование, лизинг, негосударственные пенсионные фонды, а также нефинансовые активы.</p>
												<p>Общий размер активов более 1 трлн руб. ТОП-3 среди частных российских банков по размеру активов</p>
											</div>
											<ul class="number_participants">
												<li>
													<div class="desc" id="circle15">
														<div class="circle" id="pcircles-51" data-text="Колличество нарпавленией благотворительной деятельности" data-mesure="шт <br>" data-value="9" data-postfix=""></div>
													</div>
												</li>
												<li>
													<div class="desc" id="circle25">
														<div class="circle" id="pcircles-52" data-text="Целевых программ" data-mesure="шт" data-value="37" data-postfix=""></div>
														<div class=" circle" id="pcircles-53" data-text="Охват деятельности в регионах" data-mesure="шт <br>" data-value="11" data-postfix=""></div>
													</div>
												</li>
												<li>
													<div class="desc" id="circle35">
														<div class="circle" id="pcircles-54" data-text="Объём целевого финансирования<br> на 2016г." data-mesure="млн руб <br>" data-value="923" data-postfix=""></div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>						
						</div>
						</div>
					</div>
				</div>
			</div> -->
			
<!--
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="blc_all_news">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h2>Новости</h2>
								</div>					
							</div>
							<div class="row line_years">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="blc_news_tr blc_pln">
										<div id="slider_year" class="slider_year"></div>	
										<input type="text" id="inp_year">
									</div>
									<div class="blc_news_tr blc_years_scl">
										<ul class="era_list_years">
											<li>
											</li>
											<li>
												<a href="#">весь год</a>
											</li>
											<li class="active">
												<a href="#">янв.-мар.</a>
											</li>
											<li>
												<a href="#">апр.-июн.</a>
											</li>
											<li>
												<a href="#">июл.-сен.</a>
											</li>
											<li>
												<a href="#">окт.-дек.</a>
											</li>
											<li>
											</li>
										</ul>
									</div>
									<div class="blc_news_tr blc_rss">
										<a href="#" class="rss">Подписаться на новости</a>
									</div>
								</div>					
							</div>	
							<div class="blc_link_on_news">				
								<div class="row line_two_news">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<span class="date">25 января 2016</span>
										<p>Чистая прибыль БИНБАНКа по итогам 2015 года выросла на 139%, превысив 4,5 млрд рублей</p>
										<a href="#">Подробнее</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<span class="date">25 января 2016</span>
										<p>Европлан вошел в ТОП-25 страховщиков АВТОКАСКО Компания продолжает рост и сохраняет свои ведущие позиции в автостраховании.</p>
										<a href="#">Подробнее</a>
									</div>												
								</div>	
								<div class="row line_two_news">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<span class="date">25 января 2016</span>
										<p>Чистая прибыль БИНБАНКа по итогам 2015 года выросла на 139%, превысив 4,5 млрд рублей</p>
										<a href="#">Подробнее</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<span class="date">25 января 2016</span>
										<p>Fitch подтвердил рейтинг Европлана на уровне BB</p>
										<a href="#">Подробнее</a>
									</div>												
								</div>	
							</div>							
						</div>
					</div>
				</div>
			</div>			
-->

		</div><!--blc_main_page-->
	</div><!--wrapper-->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>